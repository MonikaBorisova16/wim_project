package com.company;

import com.company.core.Engine;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactory;
import com.company.core.factory.WIMFactoryInt;

public class Main {

    public static void main(String[] args) {
        WIMFactoryInt factory = new WIMFactory();
        EngineInt engine = new Engine(factory);
        engine.start();
    }
}
