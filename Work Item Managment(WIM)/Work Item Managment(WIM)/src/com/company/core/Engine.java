package com.company.core;

import com.company.commands.CommonFunctions;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.core.contracts.ParserInt;
import com.company.core.contracts.ReaderInt;
import com.company.core.contracts.WriterInt;
import com.company.core.factory.WIMFactoryInt;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ConsoleReader;
import com.company.core.providers.ConsoleWriter;
import com.company.models.contracts.*;

import java.util.ArrayList;
import java.util.List;

public class Engine implements EngineInt {
    private static final String TERMINATION_COMMAND = "Exit";

    private ReaderInt reader;
    private WriterInt writer;
    private ParserInt parser;
    private CommonFunctionsInt finder;

    private final List<MemberInt> members;
    private final List<BugInt> bugList;
    private final List<StoryInt> storyList;
    private final List<FeedbackInt> feedbackList;
    private final List<TeamInt> teams;
    private final List<BoardInt> boards;
    private final List<CommentInt> comments;

    public Engine(WIMFactoryInt factory) {
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        parser = new CommandParser(factory, this);
        finder = new CommonFunctions(this);

        members = new ArrayList<>();
        bugList = new ArrayList<>();
        storyList = new ArrayList<>();
        feedbackList = new ArrayList<>();
        teams = new ArrayList<>();
        boards = new ArrayList<>();
        comments = new ArrayList<>();
    }

    @Override
    public List<MemberInt> getMembers() {
        return members;
    }

    @Override
    public List<BoardInt> getBoards() {
        return boards;
    }

    @Override
    public List<TeamInt> getTeams() {
        return teams;
    }

    @Override
    public List<BugInt> getBugList() {
        return bugList;
    }

    @Override
    public List<StoryInt> getStoryList() {
        return storyList;
    }

    @Override
    public List<FeedbackInt> getFeedbackList() {
        return feedbackList;
    }


    @Override
    public List<CommentInt> getComments() {
        return comments;
    }

    @Override
    public List<WorkItemsInt> getAllWorkItems(){
        List<WorkItemsInt> workitems= new ArrayList<>();
        workitems.addAll(bugList);
        workitems.addAll(storyList);
        workitems.addAll(feedbackList);
        return workitems;
    }

    @Override
    public List<AssigneWorkItemsInt> getAssigneWorkItems(){
        List<AssigneWorkItemsInt> workitems= new ArrayList<>();
        workitems.addAll(bugList);
        workitems.addAll(storyList);
        return workitems;
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }

        Command command = parser.parseCommand(commandAsString);
        List<String> parameters = parser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters,finder);
        writer.writeLine(executionResult);
    }

}
