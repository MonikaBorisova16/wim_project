package com.company.core.contracts;

public interface ReaderInt {
    String readLine();
}
