package com.company.core.contracts;

import com.company.models.contracts.*;

import java.util.List;

public interface EngineInt {
    void start();

    List<MemberInt> getMembers();

    List<TeamInt> getTeams();

    List<BoardInt> getBoards();

    List<CommentInt> getComments();

    List<BugInt> getBugList();

    List<StoryInt> getStoryList();

    List<FeedbackInt> getFeedbackList();

    List<WorkItemsInt> getAllWorkItems();

    List<AssigneWorkItemsInt> getAssigneWorkItems();

}
