package com.company.core.contracts;

public interface WriterInt {
    void write(String message);

    void writeLine(String message);
}
