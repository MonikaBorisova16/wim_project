package com.company.core.providers;

import com.company.core.contracts.WriterInt;

public class ConsoleWriter implements WriterInt {
    public void write(String message) {
        System.out.print(message);
    }

    public void writeLine(String message) {
        System.out.println(message);
    }
}
