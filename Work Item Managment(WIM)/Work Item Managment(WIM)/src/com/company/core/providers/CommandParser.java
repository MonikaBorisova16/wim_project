package com.company.core.providers;

import com.company.Constants;
import com.company.commands.ShowCommandsCommand;
import com.company.commands.add.AddCommentToAWorkItemCommand;
import com.company.commands.add.AddMemberToATeamCommand;
import com.company.commands.change.*;
import com.company.commands.contracts.Command;
import com.company.commands.creation.*;
import com.company.commands.listing.*;
import com.company.core.contracts.EngineInt;
import com.company.core.contracts.ParserInt;
import com.company.core.factory.WIMFactoryInt;

import java.util.ArrayList;
import java.util.List;

public class CommandParser implements ParserInt {


    private final WIMFactoryInt factory;
    private final EngineInt engine;

    public CommandParser(WIMFactoryInt factory, EngineInt engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public Command parseCommand(String fullCommand) {
        String commandName = fullCommand.split(" ")[0];
        return findCommand(commandName);
    }

    public List<String> parseParameters(String fullCommand) {
        String[] commandParts = fullCommand.split(" ");
        ArrayList<String> parameters = new ArrayList<>();
        for (int i = 1; i < commandParts.length; i++) {
            parameters.add(commandParts[i]);
        }
        return parameters;
    }

    private Command findCommand(String commandName) {
        switch (commandName.toLowerCase()) {
            case "createmember":
                return new CreateMemberCommand(factory, engine);
            case "createteam":
                return new CreateTeamCommand(factory, engine);
            case "createcomment":
                return new CreateCommentCommand(factory, engine);
            case "createboardinateam":
                return new CreateBoardInTeamCommand(factory, engine);
            case "createbuginaboard":
                return new CreateBugInABoardCommand(factory, engine);
            case "showcommands":
                return new ShowCommandsCommand();
            case "addmembertoateam":
                return new AddMemberToATeamCommand(factory, engine);
            case "createstoryinaboard":
                return new CreateStoryInABoardCommand(factory, engine);
            case "createfeedbackinaboard":
                return new CreateFeedbackInABoardCommand(factory, engine);
            case "addcommenttoaworkitem":
                return new AddCommentToAWorkItemCommand(factory, engine);
            case "showallmembers":
                return new ShowAllMembersCommand(engine);
            case "showallteams":
                return new ShowAllTeamsCommand(engine);
            case "showpersonsactivity":
                return new ShowPersonActivityCommand(engine);
            case "showteamsactivity":
                return new ShowTeamActivityCommand(engine);
            case "showallteammembers":
                return new ShowAllTeamMembersCommand(engine);
            case "showallteamboards":
                return new ShowAllTeamBoardsCommand(engine);
            case "showboardsactivity":
                return new ShowBoardActivityCommand(engine);
            case "changebugpriority":
                return new ChangeBugPriorityCommand(engine);
            case "changebugstatus":
                return new ChangeBugStatusCommand(engine);
            case "changebugseverity":
                return new ChangeBugSeverityCommand(engine);
            case "changestorypriority":
                return new ChangeStoryPriorityCommand(engine);
            case "changestorysize":
                return new ChangeStorySizeCommand(engine);
            case "changestorystatus":
                return new ChangeStoryStatusCommand(engine);
            case "changefeedbackrating":
                return new ChangeFeedbackRatingCommand(engine);
            case "changefeedbackstatus":
                return new ChangeFeedbackStatusCommand(engine);
            case "showallworkitems":
                return new ShowAllWorkItemsCommand(engine);
            case "sortworkitems":
                return new SortWorkItemsCommand(engine);
            case "assignworkitem":
                return new AssignCommand(engine);
            case "unassignworkitem":
                return new UnassignCommand(engine);
            case "filterworkitems":
                return new FilterWorkItemsCommand(engine);
            case "exit":
                break;

        }
        throw new IllegalArgumentException(String.format(Constants.INVALID_COMMAND, commandName));
    }
}
