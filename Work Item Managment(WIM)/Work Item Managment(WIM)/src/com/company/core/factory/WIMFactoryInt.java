package com.company.core.factory;

import com.company.models.contracts.*;

public interface WIMFactoryInt {

    MemberInt createPerson(String name);

    TeamInt createTeam(String name);

    BoardInt createBoard(String name);

    BugInt createBug(String title, String description);

    StoryInt createStory(String title, String description);

    FeedbackInt createFeedback(String title, String description);

    CommentInt createComment(String name, String comment, String author);


}
