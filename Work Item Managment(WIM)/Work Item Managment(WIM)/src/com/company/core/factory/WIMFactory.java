package com.company.core.factory;

import com.company.classes.*;
import com.company.models.contracts.*;

public class WIMFactory implements WIMFactoryInt {

    @Override
    public MemberInt createPerson(String name) {
        return new Member(name);
    }

    @Override
    public TeamInt createTeam(String name) {
        return new Team(name);
    }

    @Override
    public BoardInt createBoard(String name) {
        return new Board(name);
    }

    @Override
    public BugInt createBug(String title, String description) {
        return new Bug(title, description);
    }

    @Override
    public StoryInt createStory(String title, String description) {
        return new Story(title, description);
    }

    @Override
    public FeedbackInt createFeedback(String title, String description) {
        return new Feedback(title, description);
    }

    @Override
    public CommentInt createComment(String name, String comment, String author) {
        return new Comments(name, comment, author);
    }

}
