package com.company.commands.change;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.*;

import java.util.List;

public class ChangeFeedbackRatingCommand implements Command {
    private final EngineInt engine;

    public ChangeFeedbackRatingCommand(EngineInt engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String feedbackName;
        int wantedRating;
        String author;
        String boardName;
        String teamName;

        try {
            feedbackName = parameters.get(0);
            wantedRating = Integer.parseInt(parameters.get(1));
            author = parameters.get(2);
            boardName = parameters.get(3);
            teamName = parameters.get(4);
        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_CHANGE_FEEDBACK_RATING_COMMAND);
        }
        if (!finder.teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;
        if (!finder.boardExist(boardName)) return Constants.BOARD_DOESNT_EXIST;
        if (!finder.feedbackExist(feedbackName)) return Constants.FEEDBACK_DOESNT_EXIST;
        if (!finder.memberExist(author)) return Constants.MEMBER_DOESNT_EXIST;

        MemberInt member = finder.findMember(author);
        BoardInt board = finder.findBoard(boardName);
        TeamInt team = finder.findTeam(teamName);
        FeedbackInt feedback = finder.findFeedback(feedbackName);
        if (!finder.isBoardFromThisTeam(board, team))
            return String.format("Board %s is not in this team", board.getName());
        if (!finder.isMemberFromThisTeam(member, team))
            return String.format("Member %s is not in this team", member.getName());
        if (!finder.isWorkItemFromThisBoard(feedback, board))
            return String.format("WorkItem %s is not in this board", feedback.getTitle());


        feedback.changeRatingTo(wantedRating);
        board.addActivityHistory(String.format("Feedback %s rating is changed to %s", feedback.getTitle(), wantedRating));
        team.addActivityHistory(String.format("Feedback %s rating has been changed to %s by %s ,board %s , team %s", feedback.getTitle(), wantedRating, member.getName(), board.getName(), team.getName()));
        member.addActivityHistory(String.format("Feedback %s rating has been changed to %s by %s ,board %s , team %s", feedback.getTitle(), wantedRating, member.getName(), board.getName(), team.getName()));

        return String.format("Rating has been changed to %s", wantedRating);
    }

}
