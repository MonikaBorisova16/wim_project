package com.company.commands.change;

import com.company.Constants;
import com.company.classes.Story;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.common.Priority;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.*;

import java.util.List;

public class ChangeStoryPriorityCommand implements Command {
    private final EngineInt engine;

    public ChangeStoryPriorityCommand(EngineInt engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String storyName;
        String wantedPriority;
        String author;
        String boardName;
        String teamName;

        try {
            storyName = parameters.get(0);
            wantedPriority = parameters.get(1);
            author = parameters.get(2);
            boardName = parameters.get(3);
            teamName = parameters.get(4);
        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_CHANGE_STORY_PRIORITY_COMMAND);
        }
        if (!finder.teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;
        if (!finder.boardExist(boardName)) return Constants.BOARD_DOESNT_EXIST;
        if (!finder.storyExist(storyName)) return Constants.STORY_DOESNT_EXIST;
        if (!finder.memberExist(author)) return Constants.MEMBER_DOESNT_EXIST;

        MemberInt member = finder.findMember(author);
        BoardInt board = finder.findBoard(boardName);
        TeamInt team = finder.findTeam(teamName);
        StoryInt story = finder.findStory(storyName);
        if (!finder.isBoardFromThisTeam(board, team))
            return String.format("Board %s is not in this team", board.getName());
        if (!finder.isMemberFromThisTeam(member, team))
            return String.format("Member %s is not in this team", member.getName());

        story.changePriorityTo(Priority.valueOf(wantedPriority));
        board.addActivityHistory(String.format("Story %s priority is changed to %s by %s", story.getTitle(), wantedPriority, member.getName()));
        team.addActivityHistory(String.format("Story %s priority has been changed to %s by %s , board %s , team %s", story.getTitle(), wantedPriority, member.getName(), board.getName(), team.getName()));
        member.addActivityHistory(String.format("Story %s priority has been changed to %s by %s , board %s , team %s", story.getTitle(), wantedPriority, member.getName(), board.getName(), team.getName()));

        return String.format("Priority has been changed to %s", wantedPriority);
    }

}
