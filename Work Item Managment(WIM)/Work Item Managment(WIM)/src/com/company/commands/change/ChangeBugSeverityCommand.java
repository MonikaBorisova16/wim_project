package com.company.commands.change;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.common.Severity;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.*;

import java.util.List;

public class ChangeBugSeverityCommand implements Command {
    private final EngineInt engine;

    public ChangeBugSeverityCommand(EngineInt engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String bugName;
        String wantedSeverity;
        String author;
        String boardName;
        String teamName;

        try {
            bugName = parameters.get(0);
            wantedSeverity = parameters.get(1);
            author = parameters.get(2);
            boardName = parameters.get(3);
            teamName = parameters.get(4);
        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_CHANGE_BUG_SEVERITY_COMMAND);
        }
        if (!finder.teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;
        if (!finder.boardExist(boardName)) return Constants.BOARD_DOESNT_EXIST;
        if (!finder.bugExist(bugName)) return Constants.BUG_DOESNT_EXIST;
        if (!finder.memberExist(author)) return Constants.MEMBER_DOESNT_EXIST;

        MemberInt member = finder.findMember(author);
        BoardInt board = finder.findBoard(boardName);
        TeamInt team = finder.findTeam(teamName);
        BugInt bug = finder.findBug(bugName);
        if (!finder.isBoardFromThisTeam(board, team))
            return String.format("Board %s is not in this team", board.getName());
        if (!finder.isMemberFromThisTeam(member, team))
            return String.format("Member %s is not in this team", member.getName());

        bug.changeSeverityTo(Severity.valueOf(wantedSeverity));
        board.addActivityHistory(String.format("Bug %s severity is changed to %s by %s", bug.getTitle(), wantedSeverity, member.getName()));
        team.addActivityHistory(String.format("Bug %s from board %s changed his severity to %s by %s", bug.getTitle(), board.getName(), wantedSeverity, member.getName()));
        member.addActivityHistory(String.format("Bug %s from board %s changed his severity to %s by %s", bug.getTitle(), board.getName(), wantedSeverity, member.getName()));

        return String.format("Severity has been changed to %s", wantedSeverity);
    }

}
