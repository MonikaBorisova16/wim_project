package com.company.commands.change;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.*;

import java.util.List;

public class UnassignCommand implements Command {
    private final EngineInt engine;

    public UnassignCommand(EngineInt engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String workItemName;
        String workItemType;
        String memberName;
        String boardName;
        String teamName;

        try {
            workItemName = parameters.get(0);
            workItemType = parameters.get(1);
            memberName = parameters.get(2);
            boardName = parameters.get(3);
            teamName = parameters.get(4);

        } catch (Exception e) {
            throw new IllegalArgumentException("Faild to parse FilterWorkItems command");
        }
        if (!finder.teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;
        if (!finder.boardExist(boardName)) return Constants.BOARD_DOESNT_EXIST;
        if (!finder.memberExist(memberName)) return Constants.MEMBER_DOESNT_EXIST;

        MemberInt member = finder.findMember(memberName);
        BoardInt board = finder.findBoard(boardName);
        TeamInt team = finder.findTeam(teamName);
        if (!finder.isBoardFromThisTeam(board, team))
            return String.format("Board %s is not in this team", board.getName());
        if (!finder.isMemberFromThisTeam(member, team))
            return String.format("Member %s is not in this team", member.getName());


        switch (workItemType) {
            case "Bug": {
                if (!finder.bugExist(workItemName)) return Constants.FEEDBACK_DOESNT_EXIST;
                BugInt bug = finder.findBug(workItemName);
                if (!finder.isWorkItemFromThisBoard(bug, board))
                    return String.format("WorkItem %s is not in this board", bug.getTitle());
                bug.unassignedFromPerson(member);
                board.addActivityHistory(String.format("WorkItem %s unassigned from %s", bug.getTitle(), member.getName()));
                member.addActivityHistory(String.format("WorkItem %s unassigned from %s", bug.getTitle(), member.getName()));
                team.addActivityHistory(String.format("WorkItem %s unassigned from %s", bug.getTitle(), member.getName()));
                return String.format("WorkItem %s is unassigned from %s", bug.getTitle(), member.getName());
            }
            case "Story": {
                if (!finder.storyExist(workItemName)) return Constants.FEEDBACK_DOESNT_EXIST;
                StoryInt story = finder.findStory(workItemName);
                if (!finder.isWorkItemFromThisBoard(story, board))
                    return String.format("WorkItem %s is not in this board", story.getTitle());
                story.unassignedFromPerson(member);
                board.addActivityHistory(String.format("WorkItem %s unassigned from %s", story.getTitle(), member.getName()));
                member.addActivityHistory(String.format("WorkItem %s unassigned from %s", story.getTitle(), member.getName()));
                team.addActivityHistory(String.format("WorkItem %s unassigned from %s", story.getTitle(), member.getName()));
                return String.format("WorkItem %s is unassigned from %s", story.getTitle(), member.getName());
            }

        }
        return "Invalid switch case";
    }
}
