package com.company.commands.change;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.common.Status;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.*;

import java.util.List;

public class ChangeFeedbackStatusCommand implements Command {
    private final EngineInt engine;

    public ChangeFeedbackStatusCommand(EngineInt engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String feedbackName;
        String wantedStatus;
        String author;
        String boardName;
        String teamName;

        try {
            feedbackName = parameters.get(0);
            wantedStatus = parameters.get(1);
            author = parameters.get(2);
            boardName = parameters.get(3);
            teamName = parameters.get(4);
        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_CHANGE_FEEDBACK_STATUS_COMMAND);
        }
        if (!finder.teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;
        if (!finder.boardExist(boardName)) return Constants.BOARD_DOESNT_EXIST;
        if (!finder.feedbackExist(feedbackName)) return Constants.FEEDBACK_DOESNT_EXIST;
        if (!finder.memberExist(author)) return Constants.MEMBER_DOESNT_EXIST;

        MemberInt member = finder.findMember(author);
        BoardInt board = finder.findBoard(boardName);
        TeamInt team = finder.findTeam(teamName);
        FeedbackInt feedback = finder.findFeedback(feedbackName);
        if (!finder.isBoardFromThisTeam(board, team))
            return String.format("Board %s is not in this team", board.getName());
        if (!finder.isMemberFromThisTeam(member, team))
            return String.format("Member %s is not in this team", member.getName());

        feedback.changeStatusTo(Status.valueOf(wantedStatus));
        board.addActivityHistory(String.format("Feedback %s status is changed to %s", feedback.getTitle(), wantedStatus));
        team.addActivityHistory(String.format("Feedback %s status has been changed to %s by %s ,board %s ,team %s ", feedback.getTitle(), wantedStatus, member.getName(), board.getName(), team.getName()));
        member.addActivityHistory(String.format("Feedback %s status has been changed to %s by %s ,board %s ,team %s ", feedback.getTitle(), wantedStatus, member.getName(), board.getName(), team.getName()));

        return String.format("Status has been changed to %s", wantedStatus);
    }
}
