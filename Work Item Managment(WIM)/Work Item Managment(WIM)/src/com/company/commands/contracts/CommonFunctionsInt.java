package com.company.commands.contracts;

import com.company.models.contracts.*;

public interface CommonFunctionsInt {
    boolean isBoardFromThisTeam(BoardInt board, TeamInt team);

    boolean isMemberFromThisTeam(MemberInt member, TeamInt team);

    boolean isWorkItemFromThisBoard(WorkItemsInt workItem, BoardInt board);

    boolean memberExist(String name);

    boolean boardExist(String name);

    boolean teamExist(String name);

    TeamInt findTeam(String name);

    MemberInt findMember(String name);

    BoardInt findBoard(String name);

    BugInt findBug(String name);

    boolean bugExist(String name);

    FeedbackInt findFeedback(String name);

    boolean feedbackExist(String name);

    StoryInt findStory(String name);

    boolean storyExist(String name);
}
