package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;

import java.util.List;

public class ShowCommandsCommand implements Command {

    public String execute(List<String> parameters, CommonFunctionsInt finder) {

        String showCommands = "createmember(MemberName) \n" +
                "createcomment(CommentName, Comment, Author)\n" +
                "createteam(TeamName)\n" +
                "createboardinateam(BoardName, TeamName)\n" +
                "createbuginaboard(BugName, Description,MemberName, BoardName, TeamName)\n" +
                "createstoryinaboard(StoryName, Description,MemberName, BoardName, TeamName)\n" +
                "createfeedbackinaboard(FeedbackName, Description,MemberName, BoardName, TeamName)\n" +
                "showallmembers\n" +
                "showpersonsactivity(MemberName)\n" +
                "showallteams\n" +
                "showallteammembers(TeamName)\n" +
                "showallteamboards\n" +
                "showboardsactivity(BoardName, TeamName)\n" +
                "addmembertoateam(MemberName,TeamName)\n" +
                "addcommenttoaworkitem(CommentName, Comment, Author, WorkItemName, BoardName, TeamName)\n" +
                "changebugpriority(BugName, wantedPriority,Author, BoardName, TeamName)\n" +
                "changebugstatus(BugName, wantedStatus,Author, BoardName, TeamName)\n" +
                "changebugseverity(BugName, wantedSeverity,Author, BoardName, TeamName)\n" +
                "changestorypriority(StoryName, wantedPriority,Author, BoardName, TeamName)\n" +
                "changestorysize(StoryName, wantedSize,Author, BoardName, TeamName)\n" +
                "changestorystatus(StoryName, wantedStatus,Author, BoardName, TeamName)\n" +
                "changefeedbackrating(FeedbackName, wantedRating,Author, BoardName, TeamName)\n" +
                "changefeedbackstatus(FeedbackName, wantedStatus,Author, BoardName, TeamName)\n" +
                "listallworkitems\n" +
                "listallworkitemsbytype(Type)\n";

        return showCommands;
    }
}
