package com.company.commands.add;

import com.company.commands.contracts.Command;
import com.company.Constants;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactoryInt;
import com.company.models.contracts.MemberInt;
import com.company.models.contracts.TeamInt;

import java.util.List;

public class AddMemberToATeamCommand implements Command {
    private final WIMFactoryInt factory;
    private final EngineInt engine;


    public AddMemberToATeamCommand(WIMFactoryInt factory, EngineInt engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String memberName;
        String teamName;

        try {
            memberName = parameters.get(0);
            teamName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_ADD_MEMBER_TO_ATEAM_COMMAND);
        }
        if (!finder.memberExist(memberName)) return Constants.MEMBER_DOESNT_EXIST;
        if (!finder.teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;

        TeamInt team = finder.findTeam(teamName);
        MemberInt member = finder.findMember(memberName);

        team.addMember(member);
        member.addActivityHistory(String.format("Member %s is added to team %s ", member.getName(), team.getName()));
        team.addActivityHistory(String.format("Member %s is added to team %s", member.getName(), team.getName()));
        return String.format("Member %s is added to team %s ", member.getName(), team.getName());

    }


}
