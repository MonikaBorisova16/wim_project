package com.company.commands.add;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactoryInt;
import com.company.models.contracts.*;

import java.util.List;

public class AddCommentToAWorkItemCommand implements Command {
    private final WIMFactoryInt factory;
    private final EngineInt engine;

    public AddCommentToAWorkItemCommand(WIMFactoryInt factory, EngineInt engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String commentname;
        String author;
        String workitemname;
        String boardname;
        String teamname;

        try {
            commentname = parameters.get(0);
            author = parameters.get(1);
            workitemname = parameters.get(2);
            boardname = parameters.get(3);
            teamname = parameters.get(4);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse AddCommentToAWorkItem command parameters.");
        }
        if (!memberExist(author)) return Constants.MEMBER_DOESNT_EXIST;
        if (!teamExist(teamname)) return Constants.TEAM_DOESNT_EXIST;
        if (!workItemExist(workitemname)) return "WorkItem doesn't exist";
        if (!boardExist(boardname)) return Constants.BOARD_DOESNT_EXIST;


        TeamInt team = findTeam(teamname);
        MemberInt member = findMember(author);
        WorkItemsInt workItemsInt = findWorkItem(workitemname);
        BoardInt board = findBoard(boardname);
        CommentInt comments = findComment(commentname);
        if (!isBoardFromThisTeam(board, team)) return String.format("Board %s is not in this team", board.getName());
        workItemsInt.addComment(comments);
        board.addActivityHistory(String.format("Comment %s is added to board %s", comments.getName(), board.getName()));
        member.addActivityHistory(String.format("Comment %s is added to workitem %s in board %s from %s , team %s", comments.getName(), workItemsInt.getTitle(), board.getName(), member.getName(), team.getName()));
        return String.format("Comment \"%s\" with name \"%s\" and author \"%s\" was added to \"%s\".", comments.getComment(), comments.getName(), member.getName(), workItemsInt.getTitle());

    }

    private TeamInt findTeam(String name) {
        if (teamExist(name)) {
            for (TeamInt team : engine.getTeams()) {
                if (team.getName().equals(name)) return team;
            }
        }
        return null;

    }

    private boolean isBoardFromThisTeam(BoardInt board, TeamInt team) {
        for (BoardInt boards : team.getBoards()) {
            if (boards.getName().equals(board.getName())) {
                return true;
            }
        }
        return false;
    }

    private WorkItemsInt findWorkItem(String name) {
        if (workItemExist(name)) {
            for (WorkItemsInt workItems : engine.getAllWorkItems()) {
                if (workItems.getTitle().equals(name)) return workItems;
            }
        }
        return null;
    }

    private MemberInt findMember(String name) {
        if (memberExist(name)) {
            for (MemberInt member : engine.getMembers()) {
                if (member.getName().equals(name)) return member;
            }
        }
        return null;
    }

    private BoardInt findBoard(String name) {
        if (boardExist(name)) {
            for (BoardInt board : engine.getBoards()) {
                if (board.getName().equals(name)) return board;
            }
        }
        return null;
    }

    private CommentInt findComment(String name) {
        if (commentExist(name)) {
            for (CommentInt commentInt : engine.getComments()) {
                if (commentInt.getName().equals(name)) return commentInt;
            }
        }
        return null;
    }

    private boolean memberExist(String name) {
        for (MemberInt member : engine.getMembers()) {
            if (member.getName().equals(name)) return true;
        }
        return false;
    }

    private boolean commentExist(String name) {
        for (CommentInt comment : engine.getComments()) {
            if (comment.getName().equals(name)) return true;
        }
        return false;
    }

    private boolean boardExist(String name) {
        for (BoardInt board : engine.getBoards()) {
            if (board.getName().equals(name)) return true;
        }
        return false;
    }

    private boolean teamExist(String name) {
        for (TeamInt team : engine.getTeams()) {
            if (team.getName().equals(name)) return true;
        }
        return false;
    }

    private boolean workItemExist(String name) {
        for (WorkItemsInt workItems : engine.getAllWorkItems()) {
            if (workItems.getTitle().equals(name)) return true;
        }
        return false;
    }

}

