package com.company.commands.listing;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.BoardInt;
import com.company.models.contracts.TeamInt;

import java.util.List;

public class ShowBoardActivityCommand implements Command {
    private final EngineInt engine;

    public ShowBoardActivityCommand(EngineInt engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {

        String boardName;
        String teamName;

        try {
            boardName = parameters.get(0);
            teamName = parameters.get(1);

        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_SHOW_BOARD_ACTIVITY_COMMAND);
        }

        if (!finder.boardExist(boardName)) return Constants.BOARD_DOESNT_EXIST;
        if (!finder.teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;
        BoardInt board = finder.findBoard(boardName);
        TeamInt team = finder.findTeam(teamName);
        if (!finder.isBoardFromThisTeam(board, team))
            return String.format("Board %s is not in this team", board.getName());

        return board.showBoardActivity();

    }

}
