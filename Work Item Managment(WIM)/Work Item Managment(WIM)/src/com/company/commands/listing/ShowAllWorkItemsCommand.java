package com.company.commands.listing;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;

import java.util.List;
import java.util.stream.Collectors;

public class ShowAllWorkItemsCommand implements Command {
    private final EngineInt engine;

    public ShowAllWorkItemsCommand(EngineInt engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String type;
        try {
            type = parameters.get(0);

        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_SHOW_ALL_TEAM_MEMBERS_COMMAND);
        }

        switch (type) {
            case "Bug":
                return engine.getBugList().stream().map(Object::toString)
                        .collect(Collectors.joining("")).trim();
            case "Story":
                return engine.getStoryList().stream().map(Object::toString)
                        .collect(Collectors.joining("")).trim();
            case "Feedback":
                return engine.getFeedbackList().stream().map(Object::toString)
                        .collect(Collectors.joining("")).trim();
            case "All":
                return engine.getAllWorkItems().stream().map(Object::toString)
                        .collect(Collectors.joining("")).trim();


        }
        return "Invalid Switch Case";

    }

}
