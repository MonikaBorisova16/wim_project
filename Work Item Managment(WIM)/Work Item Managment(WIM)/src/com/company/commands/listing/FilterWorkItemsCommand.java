package com.company.commands.listing;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.common.Status;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.AssigneWorkItemsInt;
import com.company.models.contracts.WorkItemsInt;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class FilterWorkItemsCommand implements Command {
    private final EngineInt engine;

    public FilterWorkItemsCommand(EngineInt engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String filter;

        try {
            filter = parameters.get(0);

        } catch (Exception e) {
            throw new IllegalArgumentException("Faild to parse FilterWorkItems command");
        }

        switch (filter) {
            case "Fixed":
                return engine.getAllWorkItems().stream().filter(element -> element.getStatus().equals(Status.FIXED)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("]", "");
            case "Active":
                return engine.getAllWorkItems().stream().filter(element -> element.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("]", "");
            case "NotDone":
                return engine.getAllWorkItems().stream().filter(element -> element.getStatus().equals(Status.NOTDONE)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("]", "");
            case "InProgress":
                return engine.getAllWorkItems().stream().filter(element -> element.getStatus().equals(Status.INPROGRESS)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "Done":
                return engine.getAllWorkItems().stream().filter(element -> element.getStatus().equals(Status.DONE)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "New":
                return engine.getAllWorkItems().stream().filter(element -> element.getStatus().equals(Status.NEW)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "Unscheduled":
                return engine.getAllWorkItems().stream().filter(element -> element.getStatus().equals(Status.UNSCHEDULED)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "Schediled":
                return engine.getAllWorkItems().stream().filter(element -> element.getStatus().equals(Status.SCHEDULED)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "Unset":
                return engine.getAllWorkItems().stream().filter(element -> element.getStatus().equals(Status.UNSET)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "Assigne":
                return engine.getAssigneWorkItems().stream().sorted(Comparator.comparing(AssigneWorkItemsInt::getTitle)).filter(element -> !element.getAssignee().isEmpty()).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");

        }
        return "Invalid switch";
    }
}
