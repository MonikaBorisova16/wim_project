package com.company.commands.listing;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortWorkItemsCommand implements Command {
    private final EngineInt engine;

    public SortWorkItemsCommand(EngineInt engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String sortType;
        try {
            sortType = parameters.get(0);

        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_SHOW_ALL_TEAM_MEMBERS_COMMAND);
        }

        switch (sortType) {
            case "Title":
                return engine.getAllWorkItems().stream().sorted(Comparator.comparing(WorkItemsInt::getTitle)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "Size":
                return engine.getStoryList().stream().sorted(Comparator.comparing(StoryInt::getSize)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "Rating":
                return engine.getFeedbackList().stream().sorted(Comparator.comparing(FeedbackInt::getRating)).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "Severity":
                return engine.getBugList().stream().sorted(Comparator.comparing(BugInt::getSeverity)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
            case "Priority":
                return engine.getAssigneWorkItems().stream().sorted(Comparator.comparing(AssigneWorkItemsInt::getPriority)).collect(Collectors.toList()).toString().replaceAll("\\[", "").replaceAll("\\]", "");
        }
        return "Invalid Switch";
    }
}
