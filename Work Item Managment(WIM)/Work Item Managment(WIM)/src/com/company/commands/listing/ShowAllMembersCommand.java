package com.company.commands.listing;

import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;

import java.util.List;
import java.util.stream.Collectors;

public class ShowAllMembersCommand implements Command {

    private final EngineInt engine;

    public ShowAllMembersCommand(EngineInt engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
         return engine.getMembers().stream().map(Object::toString)
                .collect(Collectors.joining("")).trim();
    }
}
