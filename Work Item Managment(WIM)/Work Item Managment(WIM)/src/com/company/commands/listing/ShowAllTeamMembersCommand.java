package com.company.commands.listing;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.TeamInt;

import java.util.List;

public class ShowAllTeamMembersCommand implements Command {
    private final EngineInt engine;

    public ShowAllTeamMembersCommand(EngineInt engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String teamName;
        try {
            teamName = parameters.get(0);

        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_SHOW_ALL_TEAM_MEMBERS_COMMAND);
        }

        if (!finder.teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;
        TeamInt team = finder.findTeam(teamName);
        return team.showMembers();
    }

}
