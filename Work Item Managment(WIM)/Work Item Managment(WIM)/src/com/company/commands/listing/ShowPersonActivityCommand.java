package com.company.commands.listing;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.MemberInt;

import java.util.List;

import static com.company.Constants.MEMBER_DOESNT_EXIST;

public class ShowPersonActivityCommand implements Command {
    private final EngineInt engine;

    public ShowPersonActivityCommand(EngineInt engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {

        String memberName;

        try {
            memberName = parameters.get(0);

        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_SHOW_PERSON_ACTIVITY_COMMAND);
        }

        if (!finder.memberExist(memberName)) return MEMBER_DOESNT_EXIST;
        MemberInt member = finder.findMember(memberName);

        return member.showMemberActivity().trim();

    }


}
