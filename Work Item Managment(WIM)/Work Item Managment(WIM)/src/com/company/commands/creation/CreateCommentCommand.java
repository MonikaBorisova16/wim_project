package com.company.commands.creation;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactoryInt;
import com.company.models.contracts.CommentInt;

import java.util.List;

public class CreateCommentCommand implements Command {
    private final WIMFactoryInt factory;
    private final EngineInt engine;

    public CreateCommentCommand(WIMFactoryInt factory, EngineInt engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String name;
        String comments;
        String author;

        try {
            name = parameters.get(0);
            comments = parameters.get(1);
            author = parameters.get(2);
        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_CREATE_COMMENT_COMMAND);
        }


        CommentInt comment = factory.createComment(name, comments, author);
        engine.getComments().add(comment);

        return String.format("Comment with name %s was created.", comment.getName());
    }
}
