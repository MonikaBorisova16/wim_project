package com.company.commands.creation;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactoryInt;
import com.company.models.contracts.MemberInt;

import java.util.List;

public class CreateMemberCommand implements Command {
    private final WIMFactoryInt factory;
    private final EngineInt engine;

    public CreateMemberCommand(WIMFactoryInt factory, EngineInt engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String name;

        try {
            name = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_CREATE_MEMBER_COMMAND);
        }

        if (memberExist(name)) return "Member exist";
        MemberInt member = factory.createPerson(name);
        engine.getMembers().add(member);

        return String.format("Member with name %s was created.", member.getName());
    }

    private boolean memberExist(String name) {
        for (MemberInt member : engine.getMembers()) {
            if (member.getName().equals(name)) return true;
        }
        return false;
    }
}
