package com.company.commands.creation;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactoryInt;
import com.company.models.contracts.BoardInt;
import com.company.models.contracts.TeamInt;


import java.util.List;

public class CreateBoardInTeamCommand implements Command {
    private final WIMFactoryInt factory;
    private final EngineInt engine;

    public CreateBoardInTeamCommand(WIMFactoryInt factory, EngineInt engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String name;
        String teamName;

        try {
            name = parameters.get(0);
            teamName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_CREATE_BOARD_IN_ATEAM_COMMAND);
        }

        if (!teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;
        BoardInt board = factory.createBoard(name);
        engine.getBoards().add(board);
        TeamInt team = findTeam(teamName);
        team.addBoard(board);
        team.addActivityHistory(String.format("Board %s is created in team %s", board.getName(), team.getName()));

        return String.format("Board with name %s was created in team %s.", board.getName(), team.getName());
    }

    private boolean teamExist(String name) {
        for (TeamInt team : engine.getTeams()) {
            if (team.getName().equals(name)) return true;
        }
        return false;
    }

    private TeamInt findTeam(String name) {
        if (teamExist(name)) {
            for (TeamInt team : engine.getTeams()) {
                if (team.getName().equals(name)) return team;
            }
        }
        return null;

    }

    private boolean boardExist(String name) {
        //TODO USE STREAMS
        for (BoardInt board : engine.getBoards()) {
            if (board.getName().equals(name)) return true;
        }
        return false;
    }

}
