package com.company.commands.creation;

import com.company.Constants;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactoryInt;
import com.company.models.contracts.BoardInt;
import com.company.models.contracts.BugInt;
import com.company.models.contracts.MemberInt;
import com.company.models.contracts.TeamInt;

import java.util.List;

public class CreateBugInABoardCommand implements Command {
    private final WIMFactoryInt factory;
    private final EngineInt engine;

    public CreateBugInABoardCommand(WIMFactoryInt factory, EngineInt engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public String execute(List<String> parameters, CommonFunctionsInt finder) {
        String bugName;
        String description;
        String author;
        String boardName;
        String teamName;


        try {
            bugName = parameters.get(0);
            description = parameters.get(1);
            author = parameters.get(2);
            boardName = parameters.get(3);
            teamName = parameters.get(4);

        } catch (Exception e) {
            throw new IllegalArgumentException(Constants.FAILED_TO_PARSE_CREATE_BUG_IN_ABOARD_COMMAND);
        }

        if (!teamExist(teamName)) return Constants.TEAM_DOESNT_EXIST;
        if (!boardExist(boardName)) return Constants.BOARD_DOESNT_EXIST;
        if (!memberExist(author)) return Constants.MEMBER_DOESNT_EXIST;

        MemberInt member = findMember(author);
        BoardInt board = findBoard(boardName);
        TeamInt team = findTeam(teamName);
        if (!isBoardFromThisTeam(board, team)) return String.format("Board %s is not in this team", board.getName());
        if (!isMemberFromThisTeam(member, team))
            return String.format("Member %s is not in this team", member.getName());
        BugInt bug = factory.createBug(bugName, description);
        engine.getBugList().add(bug);

        board.addWorkItem(bug);
        member.addActivityHistory(String.format("Bug %s is created in board %s in team %s with author %s", bug.getTitle(), board.getName(), team.getName(), member.getName()));
        board.addActivityHistory(String.format("Bug %s is created in board %s in team %s with author %s", bug.getTitle(), board.getName(), team.getName(), member.getName()));
        team.addActivityHistory(String.format("Bug %s is created in board %s in team %s with author %s", bug.getTitle(), board.getName(), team.getName(), member.getName()));
        return String.format("Bug %s added to %s in team %s", bug.getTitle(), board.getName(), team.getName());
    }

    private boolean memberExist(String name) {

        for (MemberInt member : engine.getMembers()) {
            if (member.getName().equals(name)) return true;
        }
        return false;
    }

    private boolean isBoardFromThisTeam(BoardInt board, TeamInt team) {
        for (BoardInt boards : team.getBoards()) {
            if (boards.getName().equals(board.getName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isMemberFromThisTeam(MemberInt member, TeamInt team) {
        for (MemberInt members : team.getMembers()) {
            if (members.getName().equals(member.getName())) {
                return true;
            }
        }
        return false;
    }

    private boolean boardExist(String name) {

        for (BoardInt board : engine.getBoards()) {
            if (board.getName().equals(name)) return true;
        }
        return false;
    }

    private boolean teamExist(String name) {
        for (TeamInt team : engine.getTeams()) {
            if (team.getName().equals(name)) return true;
        }
        return false;
    }

    private TeamInt findTeam(String name) {
        if (teamExist(name)) {
            for (TeamInt team : engine.getTeams()) {
                if (team.getName().equals(name)) return team;
            }
        }
        return null;

    }

    private MemberInt findMember(String name) {
        if (memberExist(name)) {
            for (MemberInt member : engine.getMembers()) {
                if (member.getName().equals(name)) return member;
            }
        }
        return null;
    }

    private BoardInt findBoard(String name) {
        if (boardExist(name)) {
            for (BoardInt board : engine.getBoards()) {
                if (board.getName().equals(name)) return board;
            }
        }
        return null;
    }
}