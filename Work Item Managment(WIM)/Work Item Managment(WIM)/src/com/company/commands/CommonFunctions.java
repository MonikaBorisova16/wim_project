package com.company.commands;

import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.contracts.EngineInt;
import com.company.models.contracts.*;

public class CommonFunctions implements CommonFunctionsInt {
    private EngineInt engine;

    public CommonFunctions(EngineInt engine) {
        this.engine = engine;
    }

    @Override
    public boolean isBoardFromThisTeam(BoardInt board, TeamInt team) {
        for (BoardInt boards : team.getBoards()) {
            if (boards.getName().equals(board.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isMemberFromThisTeam(MemberInt member, TeamInt team) {
        for (MemberInt members : team.getMembers()) {
            if (members.getName().equals(member.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isWorkItemFromThisBoard(WorkItemsInt workItem, BoardInt board) {
        for (WorkItemsInt workItems : board.getWorkItemsList()) {
            if (workItems.getTitle().equals(workItem.getTitle())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean memberExist(String name) {
        //TODO USE STREAMS
        for (MemberInt member : engine.getMembers()) {
            if (member.getName().equals(name)) return true;
        }
        return false;
    }

    @Override
    public boolean boardExist(String name) {
        //TODO USE STREAMS
        for (BoardInt board : engine.getBoards()) {
            if (board.getName().equals(name)) return true;
        }
        return false;
    }

    @Override
    public boolean teamExist(String name) {
        for (TeamInt team : engine.getTeams()) {
            if (team.getName().equals(name)) return true;
        }
        return false;
    }

    @Override
    public TeamInt findTeam(String name) {
        if (teamExist(name)) {
            for (TeamInt team : engine.getTeams()) {
                if (team.getName().equals(name)) return team;
            }
        }
        return null;

    }

    @Override
    public MemberInt findMember(String name) {
        if (memberExist(name)) {
            for (MemberInt member : engine.getMembers()) {
                if (member.getName().equals(name)) return member;
            }
        }
        return null;
    }

    @Override
    public BoardInt findBoard(String name) {
        if (boardExist(name)) {
            for (BoardInt board : engine.getBoards()) {
                if (board.getName().equals(name)) return board;
            }
        }
        return null;
    }

    //methods specially for bug
    @Override
    public BugInt findBug(String name) {
        if (bugExist(name)) {
            for (BugInt bugs : engine.getBugList()) {
                if (bugs.getTitle().equals(name)) return bugs;
            }
        }
        return null;
    }

    public boolean bugExist(String name) {
        for (BugInt bugs : engine.getBugList()) {
            if (bugs.getTitle().equals(name)) return true;
        }
        return false;
    }

    //feedback
    @Override
    public FeedbackInt findFeedback(String name) {
        if (feedbackExist(name)) {
            for (FeedbackInt feedbacks : engine.getFeedbackList()) {
                if (feedbacks.getTitle().equals(name)) return feedbacks;
            }
        }
        return null;
    }

    @Override
    public boolean feedbackExist(String name) {
        for (FeedbackInt feedbacks : engine.getFeedbackList()) {
            if (feedbacks.getTitle().equals(name)) return true;
        }
        return false;
    }

    //story
    @Override
    public StoryInt findStory(String name) {
        if (storyExist(name)) {
            for (StoryInt storys : engine.getStoryList()) {
                if (storys.getTitle().equals(name)) return storys;
            }
        }
        return null;
    }

    @Override
    public boolean storyExist(String name) {
        for (StoryInt stories : engine.getStoryList()) {
            if (stories.getTitle().equals(name)) return true;
        }
        return false;
    }


}
