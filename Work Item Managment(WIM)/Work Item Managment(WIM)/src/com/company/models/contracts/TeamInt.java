package com.company.models.contracts;

import java.util.List;

public interface TeamInt {
    String getName();
    List<MemberInt> getMembers();
    List<BoardInt> getBoards();
    List<String> getActivityHistory();
    void addMember(MemberInt member);
    void addBoard(BoardInt boardInt);
    void addActivityHistory(String history);
    String showActivity();
    String showMembers();
    String showBoards();
    String toString();

}
