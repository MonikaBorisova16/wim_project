package com.company.models.contracts;

import com.company.common.Priority;

public interface AssigneWorkItemsInt extends WorkItemsInt {
    Priority getPriority();
    String getAssignee();
    String assignToPerson(MemberInt member);
    String unassignedFromPerson(MemberInt member);
    void changePriorityTo(Priority wantedPriority);

}
