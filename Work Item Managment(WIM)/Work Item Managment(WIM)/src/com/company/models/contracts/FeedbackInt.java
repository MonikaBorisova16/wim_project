package com.company.models.contracts;


public interface FeedbackInt extends WorkItemsInt {
    int getRating();
    void changeRatingTo(int rating);

}
