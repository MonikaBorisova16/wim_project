package com.company.models.contracts;

import com.company.common.Size;


public interface StoryInt extends AssigneWorkItemsInt {
    Size getSize();
    void changeSizeTo(Size size);

}
