package com.company.models.contracts;

import com.company.common.Priority;
import com.company.common.Severity;
import com.company.common.Status;


import java.util.List;

public interface BugInt extends AssigneWorkItemsInt {
    List<String> getStepsToReproduce();
    Severity getSeverity();
    void changeSeverityTo(Severity wantedSeverity);
    void changeStatusTo(Status status);
    void changePriorityTo(Priority wantedPriority);

}
