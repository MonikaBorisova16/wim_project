package com.company.models.contracts;

import com.company.common.Status;

import java.util.List;

public interface WorkItemsInt  {
    String getID();
    String getTitle();
    String getDescription();
    Status getStatus();
    List<Status> getPossibleStatus();
    String errorStatusMassage();
    List<CommentInt> getComments();
    List<String> getHistory();
    String getType();
    void changeStatusTo(Status status);
    void addComment(CommentInt comment);
    String toString();

}
