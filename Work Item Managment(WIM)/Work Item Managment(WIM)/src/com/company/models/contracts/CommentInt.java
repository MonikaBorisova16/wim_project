package com.company.models.contracts;

public interface CommentInt {

    String getName();
    String getComment();
    String getAuthor();
}
