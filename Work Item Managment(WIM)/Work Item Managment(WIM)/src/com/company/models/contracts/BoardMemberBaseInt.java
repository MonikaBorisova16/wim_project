package com.company.models.contracts;

import java.util.List;

public interface BoardMemberBaseInt {
    String getName();
    List<WorkItemsInt> getWorkItemsList();
    List<String> getActivityHistoryList();
    void addWorkItem(WorkItemsInt workItem);
    void addActivityHistory(String history);

    String toString();
}
