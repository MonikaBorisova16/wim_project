package com.company;

import java.util.Collections;

public class Constants {

    //commands
    public static final String FEEDBACK_ERROR_MESSAGE = "Feedback can be only NEW,UNSCHEDULED,SCHEDULED,DONE";
    public static final String BUG_ERROR_MESSAGE = "Bug can be only ACTIVE or FIXED!";
    public static final String STORY_ERROR_MESSAGE = "Story can be only NOT DONE, IN PROGRESS, OR DONE!";
    public static final String TEAM_DOESNT_EXIST = "Team doesn't exist";
    public static final String BOARD_DOESNT_EXIST = "Board doesn't exist";
    public static final String STORY_DOESNT_EXIST = "Story doesn't exist";
    public static final String BUG_DOESNT_EXIST = "Bug doesn't exist";
    public static final String FEEDBACK_DOESNT_EXIST = "Feedback doesn't exist";
    public static final String MEMBER_DOESNT_EXIST = "Member doesn't exist";
    public static final String FAILED_TO_PARSE_CHANGE_BUG_PRIORITY_COMMAND = "Failed to parse ChangeBugPriority command parameters.";
    public static final String FAILED_TO_PARSE_CHANGE_BUG_SEVERITY_COMMAND = "Failed to parse ChangeBugSeverity command parameters.";
    public static final String FAILED_TO_PARSE_CHANGE_BUG_STATUS_COMMAND = "Failed to parse ChangeBugStatus command parameters.";
    public static final String FAILED_TO_PARSE_CHANGE_FEEDBACK_RATING_COMMAND = "Failed to parse ChangeFeedbackRating command parameters.";
    public static final String FAILED_TO_PARSE_CHANGE_FEEDBACK_STATUS_COMMAND = "Failed to parse ChangeFeedbackStatus command parameters.";
    public static final String FAILED_TO_PARSE_CHANGE_STORY_PRIORITY_COMMAND = "Failed to parse ChangeStoryPriority command parameters.";
    public static final String FAILED_TO_PARSE_CHANGE_STORY_SIZE_COMMAND = "Failed to parse ChangeStorySize command parameters.";
    public static final String FAILED_TO_PARSE_CHANGE_STORY_STATUS_COMMAND = "Failed to parse ChangeStoryStatus command parameters.";
    public static final String FAILED_TO_PARSE_CREATE_BOARD_IN_ATEAM_COMMAND = "Failed to parse CreateBoardInATeam command parameters.";
    public static final String FAILED_TO_PARSE_CREATE_BUG_IN_ABOARD_COMMAND = "Failed to parse CreateBugInABoard command parameters.";
    public static final String FAILED_TO_PARSE_CREATE_COMMENT_COMMAND = "Failed to parse CreateComment command parameters.";
    public static final String FAILED_TO_PARSE_CREATE_FEEDBACK_IN_ABOARD_COMMAND = "Failed to parse CreateFeedbackInABoard command parameters.";
    public static final String FAILED_TO_PARSE_CREATE_MEMBER_COMMAND = "Failed to parse CreateMember command parameters.";
    public static final String FAILED_TO_PARSE_CREATE_STORY_IN_ABOARD_COMMAND = "Failed to parse CreateStoryInABoard command parameters.";
    public static final String FAILED_TO_PARSE_CREATE_TEAM_COMMAND = "Failed to parse CreateTeam command parameters.";
    public static final String FAILED_TO_PARSE_SHOW_ALL_TEAM_MEMBERS_COMMAND = "Failed to parse ShowAllTeamMembers command parameters.";
    public static final String FAILED_TO_PARSE_SHOW_BOARD_ACTIVITY_COMMAND = "Failed to parse ShowBoardActivity command parameters.";
    public static final String FAILED_TO_PARSE_SHOW_PERSON_ACTIVITY_COMMAND = "Failed to parse ShowPersonActivity command parameters.";
    public static final String FAILED_TO_PARSE_SHOW_TEAM_ACTIVITY_COMMAND = "Failed to parse ShowPersonActivity command parameters.";
    public static final String FAILED_TO_PARSE_ADD_MEMBER_TO_ATEAM_COMMAND = "Failed to parse AddMemberToATeam command parameters.";

    //commandParser
    public static final String INVALID_COMMAND = "Invalid command name: %s!";

    //tests
    public static final String STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS = "LongNameWithMoreThan15Symbols";
    public static final String STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS = "name";
    public static final String STRING_BETWEEN_10_AND_15_SYMBOLS = "LongNameString";
    public static final String STRING_WITH_MORE_THAN_500_SYMBOLS = String.join("", Collections.nCopies(501, "a"));
    public static final String STRING_WITH_MORE_THAN_50_SYMBOLS = String.join("", Collections.nCopies(51, "a"));
    public static final String BOARD_NAME = "Board1";
    public static final String MEMBER_NAME = "Member1";
    public static final String TEAM_NAME = "Team1";
    public static final String ACTIVITY_HISTORY = "Added activity history";
    public static final String EMPTY_BOARD_ACTIVITY_HISTORY = "No activity history in this board\n";
    public static final String MUST_BE_EMPTY_ACTIVITY_HISTORY = "Activity history must be empty";
    public static final String SHOULD_PRINT_ACTIVITY_HISTORY = "Activity history should print";
    public static final String MUST_BE_EMPTY_MEMBER_LIST = "Member list must be empty";
    public static final String EMPTY_MEMBER_LIST = "No members in this team\n";
    public static final String SHOULD_PRINT_MEMBER_LIST = "Member list should print";
    public static final String EMPTY_MEMBER_ACTIVITY_HISTORY = "No activity history from this member\n";
    public static final String MUST_BE_EMPTY_BOARD_LIST = "Board list must be empty";
    public static final String EMPTY_BOARD_LIST = "No boards in this team\n";
    public static final String SHOULD_PRINT_BOARD_LIST = "Member list should print";
    public static final String EMPTY_TEAM_ACTIVITY_HISTORY = "No activity history from this team\n";
    //Board
    public final static int MIN_NAME_LENGTH = 5;
    public final static int MAX_NAME_LENGTH = 10;
    public static final String WORKITEM_ADDED = "Workitem added.";
    //BoardMemberBase
    public static final int MAX_NAME_LENGTH_BOARD_MEMBER_BASE = 15;
    public static final String NAME_CANNOT_BE_NULL = "Name cannot be null";
    public static final String NAME_INVALID_NOT_IN_RANGE = "Name invalid,not in range";
    //AssigneWorkItems
    public static final String SUCCESSFULLY_ASSIGNED = "Successfully assigned";
    public static final String ALREADY_ASSIGNED_TO_SOMEBODY = "Already assigned to somebody";
    public static final String SUCCESSFULLY_UNASSIGNED = "Successfully unassigned";
    public static final String ALREADY_UNASSIGNED = "Already unassigned";
    public static final String INVALID_ASSIGNEE = "Invalid assignee";
    //WorkItems
    public final static int MIN_WORK_ITEM_TITLE_LENGTH = 10;
    public final static int MAX_WORK_ITEM_TITLE_LENGTH = 50;
    public final static int MAX_DESC_LENGTH = 500;
    public static final String LIST_CANNOT_BE_NULL = "List cannot be null";
    public static final String CHANGE_FUNCTIONS_ERROR = "Nothing to change";
    public static final String NOT_A_DECLARED_ENUM = "Not a declared enum";
    public static final String INVALID_WORK_ITEM_NAME_OR_DESCRIPTION_NOT_IN_RANGE = "Invalid work item name or description,not in range";
    //Bug
    public static final String TYPE_OF_WORK_ITEM_BUG = "Bug";
    //Feedback
    public static final String TYPE_OF_WORK_ITEM_FEEDBACK = "Feedback";
    //Story
    public static final String TYPE_OF_WORK_ITEM_STORY = "Story";
    //Team
    public static final String NO_ACTIVITY_HISTORY_IN_THIS_TEAM = "No activity history in this team.";
    public static final String NO_MEMBERS_IN_THE_LIST = "No members in the list.";
    public static final String NO_BOARDS_IN_THE_LIST = "No boards in the list.";
    public static final String MEMBER_CAN_NOT_BE_NULL = "Member can not be null";
    public static final String BOARD_CAN_NOT_BE_NULL = "Board can not be null";
}

