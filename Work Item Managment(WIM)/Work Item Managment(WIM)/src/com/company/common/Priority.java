package com.company.common;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW,
    UNSET;

    public String toString() {
        switch (this) {
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            case LOW:
                return "Low";
            case UNSET:
                return "Unset";
            default:
                throw new IllegalArgumentException();
        }
    }
}
