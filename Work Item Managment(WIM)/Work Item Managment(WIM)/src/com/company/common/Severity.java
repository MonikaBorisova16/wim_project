package com.company.common;

public enum Severity {
    CRITICAL,
    MAJOR,
    MINOR,
    SEVERITY;

    public String toString() {
        switch (this) {
            case CRITICAL:
                return "Critical";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            case SEVERITY:
                return "Severity";
            default:
                throw new IllegalArgumentException();
        }
    }
}
