package com.company.common;

public enum Size {
    LARGE,
    MEDIUM,
    SMALL,
    UNSET,
    SIZE;

    public String toString() {
        switch (this) {
            case LARGE:
                return "Large";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Small";
            case UNSET:
                return "Unset";
            case SIZE:
                return "Size";
            default:
                throw new IllegalArgumentException();
        }
    }
}
