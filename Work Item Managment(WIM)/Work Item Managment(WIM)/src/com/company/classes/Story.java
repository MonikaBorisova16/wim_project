package com.company.classes;

import com.company.Constants;
import com.company.common.Priority;
import com.company.common.Size;
import com.company.common.Status;
import com.company.models.contracts.CommentInt;
import com.company.models.contracts.StoryInt;

import java.util.ArrayList;
import java.util.List;

public class Story extends AssigneWorkItems implements StoryInt {

    private Size size;
    private boolean isAssigned;

    public Story(String title, String description) {
        super(title, description);
        size = Size.UNSET;
        isAssigned = false;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public List<Status> getPossibleStatus() {
        List<Status> list = new ArrayList<>();
        list.add(Status.NOTDONE);
        list.add(Status.INPROGRESS);
        list.add(Status.DONE);

        return list;
    }

    @Override
    public void changeStatusTo(Status wantedStatus) {
        if (getStatus().equals(wantedStatus)) {
            System.out.println(Constants.CHANGE_FUNCTIONS_ERROR);
        }
        setStatus(wantedStatus);
    }

    @Override
    public void changePriorityTo(Priority wantedPriority) {
        if (getPriority().equals(wantedPriority)) {
            System.out.println(Constants.CHANGE_FUNCTIONS_ERROR);
        }
        priority_Validation(wantedPriority);
        setPriority(wantedPriority);
    }

    @Override
    public void changeSizeTo(Size size) {
        size_Validation(size);
        setSize(size);
    }

    @Override
    public void addComment(CommentInt comment) {
        comments.add(comment);
    }

    @Override
    public String errorStatusMassage() {
        return Constants.STORY_ERROR_MESSAGE;
    }

    @Override
    public String toString() {
        return String.format("Story %s\n", getTitle());
    }

    @Override
    void setType(String type) {
        type = Constants.TYPE_OF_WORK_ITEM_STORY;
    }

    private void setSize(Size size) {
        if (size != Size.LARGE && size != Size.MEDIUM && size != Size.SMALL && size != Size.UNSET) {
            throw new IllegalArgumentException(Constants.NOT_A_DECLARED_ENUM);
        }
        this.size = size;
    }

    private static void size_Validation(Size size) {
        if (size != Size.LARGE && size != Size.MEDIUM && size != Size.SMALL && size != Size.UNSET) {
            throw new IllegalArgumentException(Constants.NOT_A_DECLARED_ENUM);
        }
    }
}
