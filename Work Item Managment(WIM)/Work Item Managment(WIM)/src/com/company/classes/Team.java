package com.company.classes;

import com.company.Constants;
import com.company.models.contracts.BoardInt;
import com.company.models.contracts.MemberInt;
import com.company.models.contracts.TeamInt;

import java.util.ArrayList;
import java.util.List;

public class Team implements TeamInt {

    private String name;
    private List<MemberInt> memberList;
    private List<BoardInt> boardList;
    private List<String> activityHistory;

    public Team(String name) {
        setName(name);
        memberList = new ArrayList<>();
        boardList = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    public List<String> getActivityHistory() {
        if (activityHistory.isEmpty()) {
            throw new IllegalArgumentException(Constants.NO_ACTIVITY_HISTORY_IN_THIS_TEAM);
        }
        return new ArrayList<>(activityHistory);
    }

    public String showActivity() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Team %s activity history: \n", getName()));

        if (activityHistory.size() != 0) {
            for (String history : activityHistory) {
                builder.append(history);
                builder.append("\n");

            }
            return builder.toString().trim();
        }
        return Constants.EMPTY_TEAM_ACTIVITY_HISTORY;
    }


    public void addActivityHistory(String history) {
        activityHistory.add(history);
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<MemberInt> getMembers() {
        if (memberList.isEmpty()) {
            throw new IllegalArgumentException(Constants.NO_MEMBERS_IN_THE_LIST);
        }
        return new ArrayList<>(memberList);
    }

    @Override
    public List<BoardInt> getBoards() {
        if (boardList.isEmpty()) {
            throw new IllegalArgumentException(Constants.NO_BOARDS_IN_THE_LIST);
        }
        return new ArrayList<>(boardList);
    }

    @Override
    public void addMember(MemberInt member) {
        if (member == null) {
            throw new IllegalArgumentException(Constants.MEMBER_CAN_NOT_BE_NULL);
        }
        memberList.add(member);
        activityHistory.add(String.format("Member %s added.", member));
    }

    public void addBoard(BoardInt board) {
        if (board == null) {
            throw new IllegalArgumentException(Constants.BOARD_CAN_NOT_BE_NULL);
        }
        boardList.add(board);
        activityHistory.add(String.format("Board %s added.", board));
    }

    public String showMembers() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Team %s members: \n", getName()));

        if (memberList.size() != 0) {
            for (MemberInt member : memberList) {
                builder.append(member.getName());
                builder.append("\n");

            }
            return builder.toString().trim();
        }
        return Constants.EMPTY_MEMBER_LIST;
    }

    public String showBoards() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Team %s boards: \n", getName()));
        if (boardList.size() != 0) {
            for (BoardInt board : boardList) {
                builder.append(board.getName());
                builder.append("\n");
            }
            return builder.toString().trim();
        }
        return Constants.EMPTY_BOARD_LIST;

    }

    @Override
    public String toString() {
        return String.format("Team's name: %s \n", getName().trim());
    }

    private void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException(Constants.NAME_CANNOT_BE_NULL);
        }
        this.name = name;
    }
}
