package com.company.classes;

import com.company.Constants;
import com.company.models.contracts.MemberInt;
import com.company.models.contracts.WorkItemsInt;

public class Member extends BoardMemberBase implements MemberInt {


    public Member(String name) {
        super(name);
    }

    @Override
    public void addWorkItem(WorkItemsInt workItem) {
        workItemsList.add(workItem);
        activityHistoryList.add(Constants.WORKITEM_ADDED);
    }

    public String showMemberActivity() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Member %s activity history: \n", getName()));

        if (activityHistoryList.size() != 0) {
            for (String history : activityHistoryList) {
                builder.append(history);
                builder.append("\n");

            }
            return builder.toString().trim();
        }
        return Constants.EMPTY_MEMBER_ACTIVITY_HISTORY;
    }

    public void addActivityHistory(String history) {
        activityHistoryList.add(history);
    }

    @Override
    public String toString() {
        return String.format("Name : %s", getName().trim());
    }

    @Override
    protected int getMinNameLength() {
        return Constants.MIN_NAME_LENGTH;
    }

    @Override
    protected int getMaxNameLength() {
        return Constants.MAX_NAME_LENGTH_BOARD_MEMBER_BASE;
    }
}
