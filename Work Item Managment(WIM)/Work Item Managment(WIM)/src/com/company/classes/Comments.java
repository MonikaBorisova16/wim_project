package com.company.classes;

import com.company.models.contracts.CommentInt;

public class Comments implements CommentInt {
    private String name;
    private String comment;
    private String author;

    public Comments(String name, String comment, String author) {
        setName(name);
        setComment(comment);
        setAuthor(author);
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public String getAuthor() {
        return author;
    }

    private void setComment(String comment) {
        this.comment = comment;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setAuthor(String author) {
        this.author = author;
    }
}
