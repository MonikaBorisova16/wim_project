package com.company.classes;

import com.company.Constants;
import com.company.common.Status;
import com.company.models.contracts.CommentInt;
import com.company.models.contracts.WorkItemsInt;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class WorkItems implements WorkItemsInt {

    private String ID;
    private String title;
    private String description;
    private Status status;
    List<CommentInt> comments;
    private List<String> history;
    private String type;

    public WorkItems(String title, String description) {
        ID = UUID.randomUUID().toString();
        setTitle(title);
        setDescription(description);
        status = Status.UNSET;
        comments = new ArrayList<>();
        history = new ArrayList<>();
        setType(type);

    }

    public String getType() {
        return type;
    }

    void setType(String type) {

        this.type = type;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<CommentInt> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    //status impl
    public abstract List<Status> getPossibleStatus();

    public abstract String errorStatusMassage();

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    void setStatus(Status status) {
        if (!getPossibleStatus().contains(status)) {
            throw new IllegalArgumentException(errorStatusMassage());
        }
        this.status = status;
    }

    private void setDescription(String description) {
        validateStringField(description, getMinLength(), getMaxDescLength());
        this.description = description;
    }

    private void setComments(List<CommentInt> comments) {
        if (comments == null) {
            throw new IllegalArgumentException(Constants.LIST_CANNOT_BE_NULL);
        }
        this.comments = comments;
    }

    private void setHistory(List<String> history) {
        if (history == null) {
            throw new IllegalArgumentException(Constants.LIST_CANNOT_BE_NULL);
        }
        this.history = history;
    }

    private static int getMinLength() {
        return Constants.MIN_WORK_ITEM_TITLE_LENGTH;
    }

    private static int getMaxTitleLength() {
        return Constants.MAX_WORK_ITEM_TITLE_LENGTH;
    }

    private static int getMaxDescLength() {
        return Constants.MAX_DESC_LENGTH;
    }

    private void setTitle(String title) {
        validateStringField(title, getMinLength(), getMaxTitleLength());
        this.title = title;
    }

    private void validateStringField(String field, int fieldMinLength, int fieldMaxLength) {
        if (field == null) {
            throw new IllegalArgumentException(Constants.NAME_CANNOT_BE_NULL);
        }
        if (field.length() < fieldMinLength || field.length() > fieldMaxLength) {
            throw new IllegalArgumentException(Constants.INVALID_WORK_ITEM_NAME_OR_DESCRIPTION_NOT_IN_RANGE);
        }
    }
}
