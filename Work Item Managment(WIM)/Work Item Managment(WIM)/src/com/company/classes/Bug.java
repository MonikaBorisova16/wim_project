package com.company.classes;

import com.company.Constants;
import com.company.common.Priority;
import com.company.common.Severity;
import com.company.common.Status;
import com.company.models.contracts.BugInt;
import com.company.models.contracts.CommentInt;

import java.util.ArrayList;
import java.util.List;

public class Bug extends AssigneWorkItems implements BugInt {

    private List<String> stepsToReproduce;
    private Severity severity;
    private boolean isAssigned;

    public Bug(String title, String description) {
        super(title, description);
        stepsToReproduce = new ArrayList<>();
        severity = Severity.MINOR;
        isAssigned = false;
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    @Override
    public List<Status> getPossibleStatus() {
        List<Status> list = new ArrayList<>();
        list.add(Status.ACTIVE);
        list.add(Status.FIXED);
        list.add(Status.UNSET);
        return list;
    }

    @Override
    public void changeStatusTo(Status status) {

        if (getStatus().equals(status)) {
            System.out.println(Constants.CHANGE_FUNCTIONS_ERROR);
        } else {
            setStatus(status);
        }
    }

    @Override
    public void changePriorityTo(Priority wantedPriority) {
        if (getPriority().equals(wantedPriority)) {
            System.out.println(Constants.CHANGE_FUNCTIONS_ERROR);
        }
        priority_Validation(wantedPriority);
        setPriority(wantedPriority);
    }

    @Override
    public void changeSeverityTo(Severity wantedSeverity) {
        if (getSeverity().equals(wantedSeverity)) {
            System.out.println(Constants.CHANGE_FUNCTIONS_ERROR);
        }
        severity_Validation(wantedSeverity);
        setSeverity(wantedSeverity);
    }

    @Override
    public void addComment(CommentInt comment) {
        comments.add(comment);
    }

    @Override
    public String errorStatusMassage() {
        return Constants.BUG_ERROR_MESSAGE;
    }

    @Override
    public String toString() {
        return String.format("Bug %s\n", getTitle());
    }

    @Override
    void setType(String type) {
        super.setType(Constants.TYPE_OF_WORK_ITEM_BUG);
    }

    private void setStepsToReproduce(List<String> stepsToReproduce) {
        if (stepsToReproduce == null) {
            throw new IllegalArgumentException(Constants.LIST_CANNOT_BE_NULL);
        }
        this.stepsToReproduce = stepsToReproduce;
    }

    private void setSeverity(Severity severity) {
        severity_Validation(severity);
        this.severity = severity;
    }

    private void severity_Validation(Severity severity) {
        if (severity != Severity.MINOR && severity != Severity.MAJOR && severity != Severity.CRITICAL) {
            throw new IllegalArgumentException(Constants.NOT_A_DECLARED_ENUM);
        }
    }
}

