package com.company.classes;

import com.company.Constants;
import com.company.common.Priority;
import com.company.models.contracts.AssigneWorkItemsInt;
import com.company.models.contracts.MemberInt;

public abstract class AssigneWorkItems extends WorkItems implements AssigneWorkItemsInt {

    private Priority priority;
    private String assignee;
    private boolean isAssigned;

    public AssigneWorkItems(String title, String description) {
        super(title, description);
        priority = Priority.UNSET;
        assignee = "";
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public String getAssignee() {
        return assignee;
    }

    @Override
    public String assignToPerson(MemberInt member) {
        if (!isAssigned()) {
            setAssigned(true);
            setAssignee(member.getName());

            return Constants.SUCCESSFULLY_ASSIGNED;
        }
        return Constants.ALREADY_ASSIGNED_TO_SOMEBODY;
    }

    @Override
    public String unassignedFromPerson(MemberInt member) {
        if (isAssigned()) {
            setAssigned(false);
            return Constants.SUCCESSFULLY_UNASSIGNED;
        }
        return Constants.ALREADY_UNASSIGNED;
    }

    void setPriority(Priority priority) {
        priority_Validation(priority);
        this.priority = priority;
    }

    void priority_Validation(Priority priority) {
        if (priority != Priority.LOW && priority != Priority.MEDIUM && priority != Priority.HIGH && priority != Priority.UNSET) {
            throw new IllegalArgumentException(Constants.NOT_A_DECLARED_ENUM);
        }
    }

    private void setAssignee(String assignee) {

        if (assignee == null) {
            throw new IllegalArgumentException(Constants.INVALID_ASSIGNEE);
        }
        this.assignee = assignee;
    }

    private boolean isAssigned() {
        return isAssigned;
    }

    private void setAssigned(boolean assigned) {
        isAssigned = assigned;
    }


}
