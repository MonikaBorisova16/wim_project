package com.company.classes;

import com.company.Constants;
import com.company.models.contracts.BoardInt;
import com.company.models.contracts.WorkItemsInt;

public class Board extends BoardMemberBase implements BoardInt {


    public Board(String name) {
        super(name);
    }

    @Override
    public void addActivityHistory(String history) {
        activityHistoryList.add(history);
    }

    public String showBoardActivity() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Board %s activity history: \n", getName()));

        if (activityHistoryList.size() != 0) {
            for (String history : activityHistoryList) {
                builder.append(history);
                builder.append("\n");

            }
            return builder.toString().trim();
        }
        return Constants.EMPTY_BOARD_ACTIVITY_HISTORY;
    }

    @Override
    public void addWorkItem(WorkItemsInt workItem) {

        workItemsList.add(workItem);
        activityHistoryList.add(Constants.WORKITEM_ADDED);
    }

    @Override
    public String toString() {
        return String.format("Name : %s", getName().trim());
    }

    @Override
    protected int getMinNameLength() {
        return Constants.MIN_NAME_LENGTH;
    }

    @Override
    protected int getMaxNameLength() {
        return Constants.MAX_NAME_LENGTH;
    }
}
