package com.company.classes;

import com.company.Constants;
import com.company.models.contracts.BoardMemberBaseInt;
import com.company.models.contracts.WorkItemsInt;

import java.util.ArrayList;
import java.util.List;

public class BoardMemberBase implements BoardMemberBaseInt {

    private String name;
    List<WorkItemsInt> workItemsList;
    List<String> activityHistoryList;

    public BoardMemberBase(String name) {
        setName(name);
        workItemsList = new ArrayList<>();
        activityHistoryList = new ArrayList<>();
    }

    @Override
    public void addWorkItem(WorkItemsInt workItem) {
    }

    @Override
    public void addActivityHistory(String history) {
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItemsInt> getWorkItemsList() {
        return new ArrayList<>(workItemsList);
    }

    @Override
    public List<String> getActivityHistoryList() {
        return new ArrayList<>(activityHistoryList);
    }

    protected int getMinNameLength() {
        return Constants.MIN_NAME_LENGTH;
    }

    protected int getMaxNameLength() {
        return Constants.MAX_NAME_LENGTH_BOARD_MEMBER_BASE;
    }

    private void setName(String name) {
        validateStringField(name, getMinNameLength(), getMaxNameLength());
        this.name = name;
    }

    private static void validateStringField(String field, int fieldMinLength, int fieldMaxLength) {
        if (field == null) {
            throw new IllegalArgumentException(Constants.NAME_CANNOT_BE_NULL);
        }
        if (field.length() < fieldMinLength || field.length() > fieldMaxLength) {
            throw new IllegalArgumentException(Constants.NAME_INVALID_NOT_IN_RANGE);
        }
    }


}
