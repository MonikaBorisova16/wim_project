package com.company.classes;

import com.company.Constants;
import com.company.common.Status;
import com.company.models.contracts.CommentInt;
import com.company.models.contracts.FeedbackInt;

import java.util.ArrayList;
import java.util.List;

public class Feedback extends WorkItems implements FeedbackInt {

    private int rating;

    public Feedback(String title, String description) {
        super(title, description);
        setRating(rating);
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public List<Status> getPossibleStatus() {
        List<Status> list = new ArrayList<>();
        list.add(Status.NEW);
        list.add(Status.UNSCHEDULED);
        list.add(Status.SCHEDULED);
        list.add(Status.DONE);
        list.add(Status.UNSET);
        return list;
    }

    @Override
    public void changeStatusTo(Status wantedStatus) {
        if (getStatus().equals(wantedStatus)) {
            System.out.println(Constants.CHANGE_FUNCTIONS_ERROR);
        }
        setStatus(wantedStatus);
    }

    @Override
    public void changeRatingTo(int rating) {
        if (getRating() == rating) {
            System.out.println(Constants.CHANGE_FUNCTIONS_ERROR);
        }
        setRating(rating);
    }

    @Override
    public void addComment(CommentInt comment) {
        comments.add(comment);
    }

    @Override
    public String errorStatusMassage() {
        return Constants.FEEDBACK_ERROR_MESSAGE;
    }

    @Override
    public String toString() {
        return String.format("Feedback %s\n", getTitle());
    }

    @Override
    void setType(String type) {
        type = Constants.TYPE_OF_WORK_ITEM_FEEDBACK;
    }

    private void setRating(int rating) {
        this.rating = rating;
    }
}

