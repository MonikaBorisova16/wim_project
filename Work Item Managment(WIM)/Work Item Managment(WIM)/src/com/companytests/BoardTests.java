package com.companytests;

import com.company.Constants;
import com.company.classes.Board;
import com.company.classes.Bug;
import com.company.models.contracts.BoardInt;
import com.company.models.contracts.BugInt;
import org.junit.Assert;
import org.junit.Test;

public class BoardTests {

    private BoardInt board = new Board(Constants.BOARD_NAME);
    private BugInt bug = new Bug("BugNameBlahBlah", "descriptionOfBugblahblah");


    @Test
    public void testBoardCreation() {
        String result = board.getName();
        Assert.assertEquals("Set method doesn't set board name right", Constants.BOARD_NAME, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBoardNameIsLongerThanMaxValue() {
        BoardInt board1 = new Board(Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBoardNameIsShorterThanMinValue() {
        BoardInt board1 = new Board(Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS);
    }

    @Test
    public void testBoardShowEmptyActivityHistory() {
        String result = board.showBoardActivity();
        Assert.assertEquals(Constants.MUST_BE_EMPTY_ACTIVITY_HISTORY, Constants.EMPTY_BOARD_ACTIVITY_HISTORY, result);
    }

    @Test
    public void testBoardShowActivityHistory() {
        board.addActivityHistory(Constants.ACTIVITY_HISTORY);
        String result = board.showBoardActivity();
        Assert.assertEquals(Constants.SHOULD_PRINT_ACTIVITY_HISTORY, "Board Board1 activity history: \nAdded activity history", result);
    }

    @Test
    public void testAddWorkItemToBoard() {
        board.addWorkItem(bug);
        String result = board.showBoardActivity();
        Assert.assertEquals("Board Board1 activity history: \nWorkitem added.", result);
    }

    //TODO test toString()
}
