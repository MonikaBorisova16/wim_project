package com.companytests.creationtests;

import com.company.commands.CommonFunctions;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.Engine;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactory;
import com.company.core.factory.WIMFactoryInt;
import com.company.core.providers.CommandParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class CreateTeamCommandTest {
    private WIMFactoryInt factory;
    private EngineInt engine;
    private CommandParser commandParser;
    private CommonFunctionsInt finder;

    @Before
    public void initFields() {
        factory = new WIMFactory();
        engine = new Engine(factory);
        commandParser = new CommandParser(factory, engine);
        finder = new CommonFunctions(engine);

    }

    @Test
    public void testCreateCommentCommand() {
        String name = "Team1";
        List<String> parameters = commandParser
                .parseParameters
                        ("createteam Team1");
        Command testCommand = commandParser.parseCommand("createteam Team1");
        String result = testCommand.execute(parameters, finder);
        Assert.assertEquals("Team with name Team1 was created.", result);
    }
}
