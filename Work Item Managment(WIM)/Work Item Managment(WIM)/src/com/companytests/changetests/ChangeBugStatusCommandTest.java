package com.companytests.changetests;

import com.company.commands.CommonFunctions;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.common.Severity;
import com.company.common.Status;
import com.company.core.Engine;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactory;
import com.company.core.factory.WIMFactoryInt;
import com.company.core.providers.CommandParser;
import com.company.models.contracts.BoardInt;
import com.company.models.contracts.BugInt;
import com.company.models.contracts.MemberInt;
import com.company.models.contracts.TeamInt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ChangeBugStatusCommandTest {
    private WIMFactoryInt factory;
    private EngineInt engine;
    private CommandParser commandParser;
    private CommonFunctionsInt finder;

    @Before
    public void initFields() {
        factory = new WIMFactory();
        engine = new Engine(factory);
        commandParser = new CommandParser(factory, engine);
        finder = new CommonFunctions(engine);

    }

    @Test
    public void testChangeBugStatusCommand() {
        MemberInt member = factory.createPerson("Member");
        engine.getMembers().add(member);
        TeamInt team = factory.createTeam("Team1");
        engine.getTeams().add(team);
        team.addMember(member);
        BoardInt board = factory.createBoard("Board1");
        engine.getBoards().add(board);
        team.addBoard(board);
        BugInt bug = factory.createBug("BugTest1234567", "TestDescription1234");
        engine.getBugList().add(bug);
        board.addWorkItem(bug);
        bug.changeStatusTo(Status.FIXED);
        List<String> parameters = commandParser.parseParameters("changebugstatus BugTest1234567 FIXED Member Board1 Team1");
        Command testCommand = commandParser.parseCommand("changebugstatus BugTest1234567 FIXED Member Board1 Team1");
        String result = testCommand.execute(parameters, finder);
        Assert.assertEquals("Status has been changed to FIXED", result);
    }
}
