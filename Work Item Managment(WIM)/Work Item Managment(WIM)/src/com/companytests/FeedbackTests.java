package com.companytests;

import com.company.Constants;
import com.company.classes.Comments;
import com.company.classes.Feedback;
import com.company.common.Status;
import com.company.models.contracts.CommentInt;
import com.company.models.contracts.FeedbackInt;
import org.junit.Assert;
import org.junit.Test;

public class FeedbackTests {
    private FeedbackInt feedback = new Feedback("FeedbackNameBlahBlah", "FeedbackDescriptionBlahBlah");
    private CommentInt comment = new Comments("Comment", "A comment", Constants.MEMBER_NAME);

    @Test
    public void testFeedbackCreationTitle() {
        String feedbackTitleResult = feedback.getTitle();
        Assert.assertEquals("Set method doesn't set feedback title right", "FeedbackNameBlahBlah", feedbackTitleResult);
    }

    @Test
    public void testFeedbackCreationDescription() {
        String feedbackDescriptionResult = feedback.getDescription();
        Assert.assertEquals("Set method doesn't set feedback description right", "FeedbackDescriptionBlahBlah", feedbackDescriptionResult);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugNameIsLongerThanMaxValue() {
        FeedbackInt feedback = new Feedback(Constants.STRING_WITH_MORE_THAN_50_SYMBOLS, Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugNameIsShorterThanMinValue() {
        FeedbackInt feedback = new Feedback(Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS, Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsLongerThanMaxValue() {
        FeedbackInt feedback = new Feedback(Constants.STRING_BETWEEN_10_AND_15_SYMBOLS, Constants.STRING_WITH_MORE_THAN_500_SYMBOLS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsShorterThanMinValue() {
        FeedbackInt feedback = new Feedback(Constants.STRING_BETWEEN_10_AND_15_SYMBOLS, Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS);
    }

    @Test
    public void testFeedbackGetRating() {
        int result = feedback.getRating();
        Assert.assertEquals(0, result);
    }

    @Test
    public void testFeedbackGetPossibleStatus() {
        Status firstElement = feedback.getPossibleStatus().get(0);
        Status secondElement = feedback.getPossibleStatus().get(1);
        Status thirdElement = feedback.getPossibleStatus().get(2);
        Status fourthElement = feedback.getPossibleStatus().get(3);
        Status fifthElement = feedback.getPossibleStatus().get(4);
        Assert.assertEquals(Status.NEW, firstElement);
        Assert.assertEquals(Status.UNSCHEDULED, secondElement);
        Assert.assertEquals(Status.SCHEDULED, thirdElement);
        Assert.assertEquals(Status.DONE, fourthElement);
        Assert.assertEquals(Status.UNSET, fifthElement);
    }

    @Test
    public void testFeedbackChangeStatusTo() {
        feedback.changeStatusTo(Status.NEW);
        Status result = feedback.getStatus();
        Assert.assertEquals(Status.NEW, result);

    }
    @Test
    public void testFeedbackChangeRatingTo (){
        feedback.changeRatingTo(10);
        int result=feedback.getRating();
        Assert.assertEquals(10,result);
    }
    @Test
    public void testFeedbackAddComment()
    {
        feedback.addComment(comment);
        CommentInt firstComment = feedback.getComments().get(0);
        Assert.assertEquals(comment,firstComment);
    }
    @Test
    public void testFeedbackErrorStatusMessage()
    {
        String result = "Feedback can be only NEW,UNSCHEDULED,SCHEDULED,DONE";
        Assert.assertEquals(feedback.errorStatusMassage(),result);
    }
    @Test
    public void testFeedbackToString()
    {
        String result = "Feedback FeedbackNameBlahBlah\n";
        Assert.assertEquals(feedback.toString(),result);

    }
}
