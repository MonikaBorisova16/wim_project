package com.companytests;

import com.company.Constants;
import com.company.classes.Bug;
import com.company.classes.Comments;
import com.company.classes.Member;
import com.company.classes.WorkItems;
import com.company.common.Priority;
import com.company.common.Severity;
import com.company.common.Status;
import com.company.models.contracts.BugInt;
import com.company.models.contracts.CommentInt;
import com.company.models.contracts.MemberInt;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BugTests {

    private BugInt bug = new Bug("BugNameBlahBlah", "BugDescriptionBlahBlah");
    private CommentInt comment =new Comments("Comment","A comment",Constants.MEMBER_NAME);
    private MemberInt member= new Member("Member");
    @Test
    public void testBugCreationTitle() {
        String bugTitleResult = bug.getTitle();
        Assert.assertEquals("Set method doesn't set bug title right", "BugNameBlahBlah", bugTitleResult);
    }

    @Test
    public void testBugCreationDescription() {
        String bugDescriptionResult = bug.getDescription();
        Assert.assertEquals("Set method doesn't set bug description right", "BugDescriptionBlahBlah", bugDescriptionResult);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugNameIsLongerThanMaxValue() {
        BugInt bug = new Bug(Constants.STRING_WITH_MORE_THAN_50_SYMBOLS, Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugNameIsShorterThanMinValue() {
        BugInt bug = new Bug(Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS, Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsLongerThanMaxValue() {
        BugInt bug = new Bug(Constants.STRING_BETWEEN_10_AND_15_SYMBOLS, Constants.STRING_WITH_MORE_THAN_500_SYMBOLS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsShorterThanMinValue() {
        BugInt bug = new Bug(Constants.STRING_BETWEEN_10_AND_15_SYMBOLS, Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS);
    }

    @Test
    public void testBugGetStepsToReproduce()
    { List<String> result = bug.getStepsToReproduce();
       Assert.assertEquals(new ArrayList<>(),result);
    }

    @Test
    public void testBugGetSeverity()
    { Severity result = bug.getSeverity();
        Assert.assertEquals(Severity.MINOR,result);
    }

    @Test
    public void testBugChangeSeverity()
    { bug.changeSeverityTo(Severity.MAJOR);
        Severity result = bug.getSeverity();
        Assert.assertEquals(Severity.MAJOR,result);
    }

    @Test
    public void testBugGetPossibleStatus()
    { Status firstElement = bug.getPossibleStatus().get(0);
        Status secondElement = bug.getPossibleStatus().get(1);
        Status thirdElement=bug.getPossibleStatus().get(2);
        Assert.assertEquals(Status.ACTIVE,firstElement);
        Assert.assertEquals(Status.FIXED,secondElement);
        Assert.assertEquals(Status.UNSET,thirdElement);
    }

    @Test
    public void testBugChangeStatus()
    {
        bug.changeStatusTo(Status.ACTIVE);
        Status result = bug.getStatus();
        Assert.assertEquals(Status.ACTIVE,result);

    }
    @Test
    public void testBugChangePriority()
    {
        bug.changePriorityTo(Priority.HIGH);
        Priority result = bug.getPriority();
        Assert.assertEquals(Priority.HIGH,result);
    }
    @Test
    public void testBugAddComment()
    {
        bug.addComment(comment);
        CommentInt firstComment = bug.getComments().get(0);
        Assert.assertEquals(comment,firstComment);
    }
    @Test
    public void testBugErrorStatusMessage()
    {
        String result = "Bug can be only ACTIVE or FIXED!";
        Assert.assertEquals(bug.errorStatusMassage(),result);
    }
    @Test
    public void testBugToString()
    {
        String result = "Bug BugNameBlahBlah\n";
        Assert.assertEquals(bug.toString(),result);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testBugSeverityValidationThrowException()
    {
        bug.changeSeverityTo(Severity.SEVERITY);
    }

    @Test
    public void testBugAssignToPerson(){
        String result = bug.assignToPerson(member);
        Assert.assertEquals("Successfully assigned",result);
    }

    @Test
    public void testBugUnassignedFromPerson(){
        bug.assignToPerson(member);
        String result = bug.unassignedFromPerson(member);
        Assert.assertEquals("Successfully unassigned",result);
    }

}
