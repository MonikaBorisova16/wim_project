package com.companytests;

import com.company.Constants;
import com.company.classes.Comments;
import com.company.models.contracts.CommentInt;
import org.junit.Assert;
import org.junit.Test;

public class CommentsTests {
    private CommentInt comment = new Comments("Comment", "A comment", Constants.MEMBER_NAME);

    @Test
    public void testCommentsCreationName() {
        String commentsTitleResult = comment.getName();
        Assert.assertEquals("Set method doesn't set comment's class name right", "Comment", commentsTitleResult);
    }
    @Test
    public void testCommentsCreationOfComment(){
        String commentsCommentResult = comment.getComment();
        Assert.assertEquals("Set method doesn't set comment's class comment right", "A comment", commentsCommentResult);
    }
    @Test
    public void testCommentsCreationOfAuthor(){
        String commentsAuthorResult = comment.getAuthor();
        Assert.assertEquals("Set method doesn't set comment's class author right", Constants.MEMBER_NAME, commentsAuthorResult);
    }
    @Test
    public void testCommentsGetName(){
        String result=comment.getName();
        Assert.assertEquals("Comment",result);
    }

    @Test
    public void testCommentsGetComment(){
        String result=comment.getComment();
        Assert.assertEquals("A comment",result);
    }

    @Test
    public void testCommentsGetAuthor(){
        String result=comment.getAuthor();
        Assert.assertEquals(Constants.MEMBER_NAME,result);
    }


}
