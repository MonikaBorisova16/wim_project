package com.companytests;

import com.company.Constants;
import com.company.classes.Comments;
import com.company.classes.Member;
import com.company.classes.Story;
import com.company.common.Priority;
import com.company.common.Size;
import com.company.common.Status;
import com.company.models.contracts.CommentInt;
import com.company.models.contracts.MemberInt;
import com.company.models.contracts.StoryInt;
import org.junit.Assert;
import org.junit.Test;


public class StoryTest{
    private StoryInt story = new Story("StoryNameBlahBlah", "StoryDescriptionBlahBlah");
    private CommentInt comment =new Comments("Comment","A comment", Constants.MEMBER_NAME);
    private MemberInt member= new Member("Member");
    @Test
    public void testStoryCreationTitle() {
        String storyTitleResult = story.getTitle();
        Assert.assertEquals("Set method doesn't set story title right", "StoryNameBlahBlah", storyTitleResult);
    }

    @Test
    public void testStoryCreationDescription() {
        String storyDescriptionResult = story.getDescription();
        Assert.assertEquals("Set method doesn't set story description right", "StoryDescriptionBlahBlah", storyDescriptionResult);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryNameIsLongerThanMaxValue() {
        StoryInt story =new Story(Constants.STRING_WITH_MORE_THAN_50_SYMBOLS,Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryNameIsShorterThanMinValue() {
        StoryInt story=new Story(Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS,Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryDescriptionIsLongerThanMaxValue() {
        StoryInt story=new Story(Constants.STRING_BETWEEN_10_AND_15_SYMBOLS,Constants.STRING_WITH_MORE_THAN_500_SYMBOLS);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryDescriptionIsShorterThanMinValue() {
        StoryInt story=new Story(Constants.STRING_BETWEEN_10_AND_15_SYMBOLS,Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackNameIsLongerThanMaxValue() {
        StoryInt story =new Story(Constants.STRING_WITH_MORE_THAN_50_SYMBOLS,Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackNameIsShorterThanMinValue() {
        StoryInt story=new Story(Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS,Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackDescriptionIsLongerThanMaxValue() {
        StoryInt story=new Story(Constants.STRING_BETWEEN_10_AND_15_SYMBOLS,Constants.STRING_WITH_MORE_THAN_500_SYMBOLS);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackDescriptionIsShorterThanMinValue() {
        StoryInt story=new Story(Constants.STRING_BETWEEN_10_AND_15_SYMBOLS,Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS);
    }

    @Test
    public void testStoryGetSize()
    { Size result = story.getSize();
        Assert.assertEquals(Size.UNSET,result);
    }
    @Test
    public void testStoryGetPossibleStatus()
    { Status firstElement = story.getPossibleStatus().get(0);
        Status secondElement = story.getPossibleStatus().get(1);
        Status thirdElement=story.getPossibleStatus().get(2);
        Assert.assertEquals(Status.NOTDONE,firstElement);
        Assert.assertEquals(Status.INPROGRESS,secondElement);
        Assert.assertEquals(Status.DONE,thirdElement);
    }
    @Test
    public void testStoryChangeStatusTo()
    {
        story.changeStatusTo(Status.NOTDONE);
        Status result = story.getStatus();
        Assert.assertEquals(Status.NOTDONE,result);

    }
    @Test
    public void testStoryChangePriority()
    {
        story.changePriorityTo(Priority.HIGH);
        Priority result = story.getPriority();
        Assert.assertEquals(Priority.HIGH,result);
    }
    @Test
    public void testStoryChangeSizeTo()
    {
        story.changeSizeTo(Size.SMALL);
        Size result= story.getSize();
        Assert.assertEquals(Size.SMALL,result);
    }
    @Test
    public void testStoryAddComment()
    {
        story.addComment(comment);
        CommentInt firstComment = story.getComments().get(0);
        Assert.assertEquals(comment,firstComment);
    }
    @Test
    public void testStoryErrorStatusMessage()
    {
        String result = "Story can be only NOT DONE, IN PROGRESS, OR DONE!";
        Assert.assertEquals(story.errorStatusMassage(),result);
    }
    @Test
    public void testStoryToString()
    {
        String result = "Story StoryNameBlahBlah\n";
        Assert.assertEquals(story.toString(),result);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testStorySizeValidationThrowException()
    {
        story.changeSizeTo(Size.SIZE);
    }

    @Test
    public void testStoryAssignToPerson(){
        String result = story.assignToPerson(member);
        Assert.assertEquals("Successfully assigned",result);
    }

    @Test
    public void testStoryUnassignedFromPerson(){
        story.assignToPerson(member);
        String result = story.unassignedFromPerson(member);
        Assert.assertEquals("Successfully unassigned",result);
    }
}
