package com.companytests.listingtests;

import com.company.commands.CommonFunctions;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.Engine;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactory;
import com.company.core.factory.WIMFactoryInt;
import com.company.core.providers.CommandParser;
import com.company.models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SortWorkItemsCommandTest {
    private WIMFactoryInt factory;
    private EngineInt engine;
    private CommandParser commandParser;
    private CommonFunctionsInt finder;

    @Before
    public void initFields() {
        factory = new WIMFactory();
        engine = new Engine(factory);
        commandParser = new CommandParser(factory, engine);
        finder = new CommonFunctions(engine);

    }

    @Test
    public void testSortWorkItemsCommand() {
        TeamInt team = factory.createTeam("Team1");
        engine.getTeams().add(team);
        MemberInt member = factory.createPerson("Member");
        engine.getMembers().add(member);
        team.addMember(member);
        BoardInt board = factory.createBoard("Board1");
        engine.getBoards().add(board);
        team.addBoard(board);
        BugInt bug = factory.createBug("BugTest1234567", "TestDescription1234");
        engine.getBugList().add(bug);
        board.addWorkItem(bug);
        StoryInt story = factory.createStory("StoryTest1234567", "TestDescription1234");
        engine.getStoryList().add(story);
        board.addWorkItem(story);
        List<String> parameters = commandParser.parseParameters("sortworkitems Title");
        Command testCommand = commandParser.parseCommand("sortworkitems Title");
        String result = testCommand.execute(parameters, finder);
        Assert.assertEquals("Bug BugTest1234567\n" +
                ", Story StoryTest1234567\n", result);
    }
}
