package com.companytests.listingtests;

import com.company.commands.CommonFunctions;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.Engine;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactory;
import com.company.core.factory.WIMFactoryInt;
import com.company.core.providers.CommandParser;
import com.company.models.contracts.BoardInt;
import com.company.models.contracts.MemberInt;
import com.company.models.contracts.TeamInt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShowAllTeamMembersCommandTest {
    private WIMFactoryInt factory;
    private EngineInt engine;
    private CommandParser commandParser;
    private CommonFunctionsInt finder;

    @Before
    public void initFields() {
        factory = new WIMFactory();
        engine = new Engine(factory);
        commandParser = new CommandParser(factory, engine);
        finder = new CommonFunctions(engine);

    }

    @Test
    public void testShowAllTeamMembersCommand() {
        TeamInt team = factory.createTeam("Team1");
        engine.getTeams().add(team);
        MemberInt member = factory.createPerson("Member");
        engine.getMembers().add(member);
        team.addMember(member);
        List<String> parameters = commandParser.parseParameters("showallteammembers Team1");
        Command testCommand = commandParser.parseCommand("showallteammembers Team1");
        String result = testCommand.execute(parameters, finder);
        Assert.assertEquals("Team Team1 members: \n" +
                "Member", result);
    }
}
