package com.companytests.listingtests;

import com.company.commands.CommonFunctions;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.Engine;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactory;
import com.company.core.factory.WIMFactoryInt;
import com.company.core.providers.CommandParser;
import com.company.models.contracts.MemberInt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShowAllMembersCommandTest {
    private WIMFactoryInt factory;
    private EngineInt engine;
    private CommandParser commandParser;
    private CommonFunctionsInt finder;

    @Before
    public void initFields() {
        factory = new WIMFactory();
        engine = new Engine(factory);
        commandParser = new CommandParser(factory, engine);
        finder = new CommonFunctions(engine);

    }

    @Test
    public void testShowAllMembersCommand() {
        MemberInt member = factory.createPerson("Member1");
        engine.getMembers().add(member);
        List<String> parameters = commandParser.parseParameters("showallmembers");
        Command testCommand = commandParser.parseCommand("showallmembers");
        String result = testCommand.execute(parameters, finder);
        Assert.assertEquals("Name : Member1", result);
    }

}
