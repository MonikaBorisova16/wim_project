package com.companytests.listingtests;

import com.company.commands.CommonFunctions;
import com.company.commands.contracts.Command;
import com.company.commands.contracts.CommonFunctionsInt;
import com.company.core.Engine;
import com.company.core.contracts.EngineInt;
import com.company.core.factory.WIMFactory;
import com.company.core.factory.WIMFactoryInt;
import com.company.core.providers.CommandParser;
import com.company.models.contracts.TeamInt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShowAllTeamsCommandTest {
    private WIMFactoryInt factory;
    private EngineInt engine;
    private CommandParser commandParser;
    private CommonFunctionsInt finder;

    @Before
    public void initFields() {
        factory = new WIMFactory();
        engine = new Engine(factory);
        commandParser = new CommandParser(factory, engine);
        finder = new CommonFunctions(engine);

    }

    @Test
    public void testShowAllTeamsCommand() {
        TeamInt team = factory.createTeam("Team1");
        engine.getTeams().add(team);
        List<String> parameters = commandParser.parseParameters("showallteams");
        Command testCommand = commandParser.parseCommand("showallteams");
        String result = testCommand.execute(parameters, finder);
        Assert.assertEquals("Team's name: Team1", result);
    }
}
