package com.companytests;

import com.company.Constants;
import com.company.classes.Bug;
import com.company.classes.Member;
import com.company.models.contracts.BugInt;
import com.company.models.contracts.MemberInt;
import org.junit.Assert;
import org.junit.Test;

public class MemberTests {
    private MemberInt member = new Member(Constants.MEMBER_NAME);
    private BugInt bug = new Bug("BugNameBlahBlah", "descriptionOfBugblahblah");


    @Test
    public void testMemberCreation() {
        String result = member.getName();
        Assert.assertEquals("Set method doesn't set member name right", Constants.MEMBER_NAME, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenMemberNameIsLongerThanMaxValue() {
        MemberInt member = new Member(Constants.STRING_WITH_LENGTH_MORE_THAN_15_SYMBOLS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenMemberNameIsShorterThanMinValue() {
        MemberInt member = new Member(Constants.STRING_WITH_LENGTH_LESS_THAN_5_SYMBOLS);
    }

    @Test
    public void testMemberShowEmptyActivityHistory() {
        MemberInt member = new Member(Constants.MEMBER_NAME);
        String result = member.showMemberActivity();
        Assert.assertEquals(Constants.MUST_BE_EMPTY_ACTIVITY_HISTORY, Constants.EMPTY_MEMBER_ACTIVITY_HISTORY, result);
    }

    @Test
    public void testMemberShowActivityHistory() {
        MemberInt member = new Member(Constants.MEMBER_NAME);
        member.addActivityHistory(Constants.ACTIVITY_HISTORY);
        String result = member.showMemberActivity();
        Assert.assertEquals(Constants.SHOULD_PRINT_ACTIVITY_HISTORY, "Member Member1 activity history: \nAdded activity history", result);
    }

    @Test
    public void testAddWorkItemToBoard() {
        member.addWorkItem(bug);
        String result = member.showMemberActivity();
        Assert.assertEquals("Member Member1 activity history: \nWorkItem added.", result);
    }

    //TODO toString()
}
