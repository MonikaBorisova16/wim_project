package com.companytests;

import com.company.Constants;
import com.company.classes.Board;
import com.company.classes.Member;
import com.company.classes.Team;
import com.company.models.contracts.BoardInt;
import com.company.models.contracts.MemberInt;
import com.company.models.contracts.TeamInt;
import org.junit.Assert;
import org.junit.Test;

public class TeamTests {

    private TeamInt team = new Team(Constants.TEAM_NAME);
    private BoardInt board = new Board(Constants.BOARD_NAME);
    private MemberInt member = new Member(Constants.MEMBER_NAME);

    @Test
    public void testTeamCreation() {
        String result = team.getName();
        Assert.assertEquals("Set method doesn't set team name right", Constants.TEAM_NAME, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTeamNameNull() {
        TeamInt teamInt = new Team(null);
    }

    @Test
    public void testTeamShowEmptyActivityHistory() {
        String result = team.showActivity();
        Assert.assertEquals(Constants.MUST_BE_EMPTY_ACTIVITY_HISTORY, Constants.EMPTY_TEAM_ACTIVITY_HISTORY, result);
    }

    @Test
    public void testTeamShowActivityHistory() {
        team.addActivityHistory(Constants.ACTIVITY_HISTORY);
        String result = team.showActivity();
        Assert.assertEquals(Constants.SHOULD_PRINT_ACTIVITY_HISTORY, "Team Team1 activity history: \nAdded activity history", result);
    }

    @Test
    public void testTeamShowNoMembers() {
        String result = team.showMembers();
        Assert.assertEquals(Constants.MUST_BE_EMPTY_MEMBER_LIST, Constants.EMPTY_MEMBER_LIST, result);
    }

    @Test
    public void testTeamShowMembers() {
        team.addMember(member);
        String result = team.showMembers();
        Assert.assertEquals(Constants.SHOULD_PRINT_MEMBER_LIST, "Team Team1 members: \nMember1", result);
    }

    @Test
    public void testTeamShowNoBoards() {
        String result = team.showBoards();
        Assert.assertEquals(Constants.MUST_BE_EMPTY_BOARD_LIST, Constants.EMPTY_BOARD_LIST, result);
    }

    @Test
    public void testTeamShowBoards() {
        team.addBoard(board);
        String result = team.showBoards();
        Assert.assertEquals(Constants.SHOULD_PRINT_BOARD_LIST, "Team Team1 boards: \nBoard1", result);
    }

}
